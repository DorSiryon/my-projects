﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Resources;

namespace TriviaClientSolution
{
    public partial class App : Application
    {
        public static Communicator com;
        public static string username;
        private static Window currWnd;
        private static LoginWindow loginWnd = new LoginWindow();
        private static SignupWindow signupWnd = new SignupWindow();
        private static MenuWindow menuWnd = new MenuWindow();
		private static CreateRoomWindow createWnd = new CreateRoomWindow();
		private static ManagerWaitingRoomWindow managerWaitingWnd = new ManagerWaitingRoomWindow();
		private static WaitingRoomWindow waitingWnd = new WaitingRoomWindow();
		private static JoinRoomWindow joinWnd = new JoinRoomWindow();
		private static BestScoresWindow bestWnd = new BestScoresWindow();
		private static MyStatusWindow statusWnd = new MyStatusWindow();
		public static GameWindow gameWnd = new GameWindow();

		private void Application_Startup(object sender, StartupEventArgs e)
        {
			com = new Communicator();
            com.EstablishConnection();
            currWnd = loginWnd;
            currWnd.Show();
        }

        public static void GoToLogin()
        {
            currWnd.Hide();
            currWnd = loginWnd;
			loginWnd.Reset();
            currWnd.Show();
        }

        public static void GoToSignup()
        {
            currWnd.Hide();
            currWnd = signupWnd;
			signupWnd.Reset();
            currWnd.Show();
        }

        public static void GoToMenu()
        {
            currWnd.Hide();
            currWnd = menuWnd;
            menuWnd.UpdateWindow();
            currWnd.Show();
        }

		public static void GoToCreateRoom()
		{
			currWnd.Hide();
			currWnd = createWnd;
			createWnd.Reset();
			currWnd.Show();
		}

		public static void GoToManagerWaitingRoom()
		{
			currWnd.Hide();
			currWnd = managerWaitingWnd;
			managerWaitingWnd.Reset();
			managerWaitingWnd.UpdateWindow();
			currWnd.Show();
		}

		public static void GoToWaitingRoom()
		{
			currWnd.Hide();
			currWnd = waitingWnd;
			waitingWnd.Reset();
			waitingWnd.UpdateWindow();
			currWnd.Show();
		}

		public static void GoToJoinRoom()
		{
			currWnd.Hide();
			currWnd = joinWnd;
			joinWnd.Reset();
			joinWnd.UpdateWindow();
			currWnd.Show();
		}

		public static void GoToBestScores() 
		{
			currWnd.Hide();
			currWnd = bestWnd;
			bestWnd.UpdateWindow();
			currWnd.Show();
		}

		public static void GoToMyStatus()
		{
			currWnd.Hide();
			currWnd = statusWnd;
			statusWnd.UpdateWindow();
			currWnd.Show();
		}

		public static void GoToGame()
		{
			currWnd.Hide();
			currWnd = gameWnd;
			gameWnd.UpdateWindow();
			currWnd.Show();
			gameWnd.Game();
		}

		public static LogoutResponse Logout()
		{
			App.com.SendMessageWithoutData((uint)Codes.LogoutCode);
			LogoutResponse response = (LogoutResponse)App.com.ReceiveMessage();
			return response;
		}

		public static void ExitGame()
		{
			LogoutResponse response = Logout();
			if (response.status == 1)
			{
				Environment.Exit(0);
			}
		}

		public static Boolean RetryConnection()
        {
            Boolean retry = false;
            MessageBoxResult retryConnection = MessageBox.Show("Can't Connect To Server!" + Environment.NewLine +
            "Try to contact the server owner if you keep having this problem." + Environment.NewLine + "Retry Connection?", "ERROR!", MessageBoxButton.YesNo);

            switch (retryConnection)
            {
                case MessageBoxResult.Yes:
                    retry = true;
                    break;
                case MessageBoxResult.No:
                    retry = false;
                    break;
            }
            return retry;
        }
    }
}
