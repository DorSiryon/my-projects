﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClientSolution
{
	/// <summary>
	/// Interaction logic for MyStatusWindow.xaml
	/// </summary>
	public partial class MyStatusWindow : Window
	{
		public MyStatusWindow()
		{
			InitializeComponent();
		}

		public void UpdateWindow()
		{
			App.com.SendMessageWithoutData((uint)Codes.UserStatusCode);
			UserStatusResponse response = (UserStatusResponse)App.com.ReceiveMessage();

			if (response.status == 1)
			{
				TB_Avg.Text = response.results.averageAnswerTime.ToString();
				TB_Games.Text = response.results.gamesNum.ToString();
				TB_Right.Text = response.results.correctAnswerCount.ToString();
				TB_Wrong.Text = response.results.wrongAnswerCount.ToString();
			}
		}

		private void B_Back(object sender, RoutedEventArgs e)
		{
			App.GoToMenu();
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			App.ExitGame();
		}
	}
}
