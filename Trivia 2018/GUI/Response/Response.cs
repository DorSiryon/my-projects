﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

namespace TriviaClientSolution
{
    //Need to be in the Room.xmal
    public struct RoomData
    {
        public uint id;
        public string name;
        public uint maxPlayer;
        public uint timePerQuestion;
        public uint isActive;
    };

    //Need to be in the Room.xmal
    public class Highscore
    {
        public string username;
        public uint score;
    }

    public class Response
    {
        public Response(uint idRes)
        {
            id = idRes;
        }

        public uint id;
    }

    public class LoginResponse : Response
    {
        public LoginResponse() : base((uint)Codes.LoginCode)
        {
        }

        public uint status;
    }

    public class SignupResponse : Response
    {
        public SignupResponse() : base((uint)Codes.SignupCode)
        {
        }
        public uint status;
    }

    public class ErrorResponse : Response
    {
        public ErrorResponse() : base((uint)Codes.ErrorCode)
        {
        }
        public string message;
    }

    public class LogoutResponse : Response
    {
        public LogoutResponse() : base((uint)Codes.LogoutCode)
        {
        }
        public uint status;
    }


    public class GetRoomsResponse : Response
    {
        public GetRoomsResponse() : base((uint)Codes.GetRoomsCode)
        {
        }
        public uint status;
        public List<RoomData> rooms;
    }

    public class GetPlayersInRoomResponse : Response
    {
        public GetPlayersInRoomResponse() : base((uint)Codes.GetPlayersInRoomCode)
        {
        }
        public uint status;
        public List<string> players;
    }

    public class HighscoreResponse : Response
    {
        public HighscoreResponse() : base((uint)Codes.HighscoreCode)
        {
        }
        public uint status;
        public List<Highscore> highscores;
    }

    public class JoinRoomResponse : Response
    {
        public JoinRoomResponse() : base((uint)Codes.JoinRoomCode)
        {
        }
        public uint status;
    }

    public class CreateRoomResponse : Response
    {
        public CreateRoomResponse() : base((uint)Codes.CreateRoomCode)
        {
        }
        public uint status;
    }

	public class CloseRoomResponse : Response
	{
		public CloseRoomResponse() : base((uint)Codes.CloseRoomCode)
		{
		}
		public uint status;
	};

	public class StartGameResponse : Response
	{
		public StartGameResponse() : base((uint)Codes.StartGameCode) 
		{ }
		public uint status;
	};

	public class GetRoomStateResponse : Response
	{
		public GetRoomStateResponse() : base((uint)Codes.GetRoomStateCode) 
		{ }
		public uint status;
		public uint hasGameBegun;
		public string roomName;
		public uint maxPlayers;
		public List<string> players;
		public uint questionCount;
		public uint answerTimeout;
	};

	public class LeaveRoomResponse : Response
	{
		public LeaveRoomResponse() : base((uint)Codes.LeaveRoomCode)
		{ }
		public uint status;
	};

	public class LeaveGameResponse : Response
	{
		public LeaveGameResponse() : base((uint)Codes.LeaveGameCode)
		{ }
		public uint status;
	};

	public class GetQuestionResponse : Response
	{
		public GetQuestionResponse() : base((uint)Codes.GetQuestionCode)
		{ }
		public uint status;
		public string question;
		public Dictionary<uint, string> answers;
	};

	public class SubmitAnswerResponse : Response
	{
		public SubmitAnswerResponse() : base((uint)Codes.SubmitAnswerCode)
		{ }
		public uint status;
		public uint correctAnswerId;
	};

	public class GetGameResultsResponse : Response 
	{
		public GetGameResultsResponse() : base((uint)Codes.GetGameResultsCode)
		{ }
		public uint status;
		public Dictionary<string, uint> results;
	};

	public class UserStatusResponse : Response
	{
		public UserStatusResponse() : base((uint)Codes.UserStatusCode)
		{ }
		public uint status;
		public PlayerResults results;
	};

	public struct PlayerResults
	{
		public string username;
		public uint gamesNum;
		public uint correctAnswerCount;
		public uint wrongAnswerCount;
		public uint averageAnswerTime;
	};

	public class Deserializer
    {
        public static Response DeserializeResponse(string msg, uint id)
        {
            Response res = JsonConvert.DeserializeObject<Response>(msg, new ResponseConverter(id));
            return res;
        }
    }

    public class ResponseConverter : CustomCreationConverter<Response>
    {
        private uint _id;

        public ResponseConverter(uint id)
        {
            _id = id;
        }

        public override Response Create(Type objectType)
        {
            switch (_id)
            {
                case (uint)Codes.ErrorCode:
                    return new ErrorResponse();

                case (uint)Codes.SignupCode:
                    return new SignupResponse();

                case (uint)Codes.LoginCode:
                    return new LoginResponse();

                case (uint)Codes.LogoutCode:
                    return new LogoutResponse();

                case (uint)Codes.GetRoomsCode:
                    return new GetRoomsResponse();

                case (uint)Codes.GetPlayersInRoomCode:
                    return new GetPlayersInRoomResponse();

                case (uint)Codes.JoinRoomCode:
                    return new JoinRoomResponse();

                case (uint)Codes.CreateRoomCode:
                    return new CreateRoomResponse();

                case (uint)Codes.HighscoreCode:
                    return new HighscoreResponse();

				case (uint)Codes.CloseRoomCode:
					return new CloseRoomResponse();

				case (uint)Codes.StartGameCode:
					return new StartGameResponse();

				case (uint)Codes.GetRoomStateCode:
					return new GetRoomStateResponse();

				case (uint)Codes.LeaveRoomCode:
					return new LeaveRoomResponse();

				case (uint)Codes.LeaveGameCode:
					return new LeaveGameResponse();

				case (uint)Codes.GetQuestionCode:
					return new GetQuestionResponse();

				case (uint)Codes.SubmitAnswerCode:
					return new SubmitAnswerResponse();

				case (uint)Codes.GetGameResultsCode:
					return new GetGameResultsResponse();

				case (uint)Codes.UserStatusCode:
					return new UserStatusResponse();

				default:
                    return null;
            }
        }

    }



//    public abstract class JsonCreationConverter<T> : JsonConverter
//    {
//        // <summary>
//        // Create an instance of objectType, based properties in the JSON object
//        // </summary>
//        // <param name="objectType">type of object expected</param>
//        // <param name="jObject">
//        // contents of JSON object that will be deserialized
//        // </param>
//        // <returns></returns>
//        protected abstract T Create(Type objectType, JObject jObject);

//        public override bool CanConvert(Type objectType)
//        {
//            return typeof(T).IsAssignableFrom(objectType);
//        }

//        public override bool CanWrite
//        {
//            get { return false; }
//        }

//        public override object ReadJson(JsonReader reader,
//                                        Type objectType,
//                                         object existingValue,
//                                         JsonSerializer serializer)
//        {
//            // Load JObject from stream
//            JObject jObject = JObject.Load(reader);

//            // Create target object based on JObject
//            T target = Create(objectType, jObject);

//            // Populate the object properties
//            serializer.Populate(jObject.CreateReader(), target);

//            return target;
//        }
//    }
}
