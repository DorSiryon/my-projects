﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;

namespace TriviaClientSolution
{
    /// <summary>
    /// Interaction logic for WaitingRoom.xaml
    /// </summary>
    public partial class WaitingRoomWindow : Window
    {
		private static BackgroundWorker listUpdater = new BackgroundWorker();
		private static List<Participants> participantsList = new List<Participants>();

		public WaitingRoomWindow()
        {
            InitializeComponent();
			L_users.ItemsSource = participantsList;
			listUpdater.WorkerReportsProgress = true;
			listUpdater.WorkerSupportsCancellation = true;
			listUpdater.DoWork += GetUpdate;
			listUpdater.ProgressChanged += UpdateList;
			listUpdater.RunWorkerCompleted += Finished;
		}

		public void UpdateWindow()
		{
			App.com.SendMessageWithoutData((uint)Codes.GetRoomStateCode);
			GetRoomStateResponse response = (GetRoomStateResponse)App.com.ReceiveMessage();
			if (response.status == 1)
			{
				TB_maxPlayers.Text = response.maxPlayers.ToString();
				TB_questionNum.Text = response.questionCount.ToString();
				TB_RoomName.Text = response.roomName;
				TB_timePerQuestion.Text = response.answerTimeout.ToString();
			}

			foreach (string name in response.players)
			{
				participantsList.Add(new Participants() { Name = name });
			}

			listUpdater.RunWorkerAsync();
		}

		private void GetUpdate(object sender, DoWorkEventArgs e) //do work
		{
			GetRoomStateResponse response;
			while (!listUpdater.CancellationPending)
			{
				App.com.SendMessageWithoutData((uint)Codes.GetRoomStateCode);
				response = (GetRoomStateResponse)App.com.ReceiveMessage();

				if (response.status == 0)
				{
					e.Cancel = false;
					return;
				}

				if (response.hasGameBegun == 1)
				{
					//transfer data to game
					App.gameWnd.roomName = response.roomName;
					App.gameWnd.questionCount = response.questionCount;
					App.gameWnd.time = (int)response.answerTimeout;
					e.Cancel = true;
					return;
				}

				if (response.players.Count != L_users.Items.Count)
				{
					participantsList.Clear();
					foreach (string name in response.players)
					{
						participantsList.Add(new Participants() { Name = name });
					}
					listUpdater.ReportProgress(1);
				}
				Thread.Sleep(200);
			}
		}

		private void UpdateList(object sender, ProgressChangedEventArgs e) //progress changed
		{
			//update list
			L_users.ItemsSource = null;
			L_users.ItemsSource = participantsList;
		}

		private void Finished(object sender, RunWorkerCompletedEventArgs e)
		{
			if (!e.Cancelled)
			{
				App.GoToMenu();
			}
			else
			{
				App.GoToGame();
			}
		}

		private void B_Leave(object sender, RoutedEventArgs e)
		{
			listUpdater.CancelAsync();
			App.com.SendMessageWithoutData((uint)Codes.LeaveRoomCode);
			LeaveRoomResponse response = (LeaveRoomResponse)App.com.ReceiveMessage();
			if (response.status == 1)
			{
				App.GoToMenu();
			}
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			BN_Leave.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
			App.ExitGame();
		}

		public void Reset()
		{
			participantsList.Clear();
			L_users.ItemsSource = null;
			L_users.ItemsSource = participantsList;
			TB_maxPlayers.Text = "";
			TB_questionNum.Text = "";
			TB_RoomName.Text = "";
			TB_timePerQuestion.Text = "";
		}
	}
}
