﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;

namespace TriviaClientSolution
{
	/// <summary>
	/// Interaction logic for GameWindow.xaml
	/// </summary>
	public partial class GameWindow : Window
	{
		private SubmitAnswerRequest request = new SubmitAnswerRequest();
		private SubmitAnswerResponse answerResponse;
		private GetQuestionResponse questionResponse;
		private DispatcherTimer gameTimer = new DispatcherTimer();
		private DispatcherTimer colorTimer = new DispatcherTimer();
		public string roomName;
		public uint questionCount;
		private int currTime;
		public int time;
		private uint score;
		private uint currQuestion;
		private uint answerTime;
		private string results;

		public GameWindow()
		{
			InitializeComponent();
			//set all the timers
			gameTimer.Interval = new TimeSpan(0, 0, 1);
			gameTimer.Tick += new EventHandler(UpdateTimer);
			colorTimer.Interval = new TimeSpan(0, 0, 0, 0, 400);
			colorTimer.Tick += new EventHandler(UpdateColor);
			ResetBackground();
		}

		private void ResetBackground()
		{
			BN_Ans1.Background = Brushes.Azure;
			BN_Ans2.Background = Brushes.Azure;
			BN_Ans3.Background = Brushes.Azure;
			BN_Ans4.Background = Brushes.Azure;
		}

		public void Game()
		{
			if (GetQuestion())
			{
				UpdateScore();
				UpdateQuestion();
				currTime = time;
				EnableButtons();
				gameTimer.Start();
			}
		}

		private void UpdateQuestion()
		{
			TB_QuestionNum.Text = currQuestion.ToString() + "/" + questionCount.ToString();
		}

		private void UpdateScore()
		{
			TB_Score.Text = score.ToString() + "/" + currQuestion.ToString();
		}

		private void UpdateColor(object sender, EventArgs e)
		{
			colorTimer.Stop();
			ResetBackground();
			Game();
		}

		//happens every timer tick
		private void UpdateTimer(object sender, EventArgs e)
		{
			answerTime = (uint)DateTime.Now.Second;
			currTime--;
			LB_Timer.Content = currTime;
			
			if ((int)LB_Timer.Content == 0)
			{
				gameTimer.Stop();
				request.answerId = 0;
				request.answerTime = (uint)time;
				Submit();
			}
		}

		public void UpdateWindow()
		{
			TB_RoomName.Text = roomName;
			TB_Question.Text = "";
			TB_QuestionNum.Text = questionCount.ToString();
			TB_Score.Text = "0";
			TB_User.Text = App.username;
			score = 0;
			currQuestion = 0;
			answerTime = 0;
			results = "";
			currTime = time;
		}

		private bool GetQuestion()
		{
			App.com.SendMessageWithoutData((uint)Codes.GetQuestionCode);
			questionResponse = (GetQuestionResponse)App.com.ReceiveMessage();
			if (questionResponse.status == 1)
			{
				TB_Question.Text = questionResponse.question;
				currQuestion++;
				foreach (KeyValuePair<uint, string> answer in questionResponse.answers)
				{
					switch (answer.Key)
					{
						case 1:
							BN_Ans1.Content = answer.Value;
							break;
						case 2:
							BN_Ans2.Content = answer.Value;
							break;
						case 3:
							BN_Ans3.Content = answer.Value;
							break;
						case 4:
							BN_Ans4.Content = answer.Value;
							break;
					}
				}
				return true;
			}
			else
			{
				FinishGame();
				return false;
			}
		}

		private void FinishGame()
		{
			App.com.SendMessageWithoutData((uint)Codes.GetGameResultsCode);
			GetGameResultsResponse gameResult = (GetGameResultsResponse)App.com.ReceiveMessage();
			if (gameResult.status == 1)
			{
				foreach (KeyValuePair<string, uint> user in gameResult.results)
				{
					results += user.Key + " - " + user.Value + Environment.NewLine;
				}

				MessageBoxResult finishGame = MessageBox.Show(results, "Game Results", MessageBoxButton.OK);
				switch (finishGame)
				{
					case MessageBoxResult.OK:
						BN_Exit.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
						break;
					default:
						break;
				}
			}
		}

		private void DisableButtons()
		{
			BN_Ans1.IsEnabled = false;
			BN_Ans2.IsEnabled = false;
			BN_Ans3.IsEnabled = false;
			BN_Ans4.IsEnabled = false;
		}

		private void EnableButtons()
		{
			BN_Ans1.IsEnabled = true;
			BN_Ans2.IsEnabled = true;
			BN_Ans3.IsEnabled = true;
			BN_Ans4.IsEnabled = true;
		}

		private void BN_Ans1_Click(object sender, RoutedEventArgs e)
		{
			gameTimer.Stop();
			DisableButtons();
			request.answerId = 1;
			request.answerTime = answerTime;
			Submit();
		}

		private void BN_Ans2_Click(object sender, RoutedEventArgs e)
		{
			gameTimer.Stop();
			DisableButtons();
			request.answerId = 2;
			request.answerTime = answerTime;
			Submit();
		}

		private void BN_Ans3_Click(object sender, RoutedEventArgs e)
		{
			gameTimer.Stop();
			DisableButtons();
			request.answerId = 3;
			request.answerTime = answerTime;
			Submit();
		}

		private void BN_Ans4_Click(object sender, RoutedEventArgs e)
		{
			gameTimer.Stop();
			DisableButtons();
			request.answerId = 4;
			request.answerTime = answerTime;
			Submit();
		}

		private void Submit()
		{
			App.com.SendMessage<SubmitAnswerRequest>(ref request, (uint)Codes.SubmitAnswerCode);
			answerResponse = (SubmitAnswerResponse)App.com.ReceiveMessage();
			if (answerResponse.status == 1)
			{
				if (answerResponse.correctAnswerId == request.answerId)
				{
					RightAns();
				}
				else
				{
					WrongAns();
				}
				colorTimer.Start();
			}
		}

		private void RightAns()
		{
			switch (answerResponse.correctAnswerId)
			{
				case 1:
					BN_Ans1.Background = Brushes.Green;
					break;
				case 2:
					BN_Ans2.Background = Brushes.Green;
					break;
				case 3:
					BN_Ans3.Background = Brushes.Green;
					break;
				case 4:
					BN_Ans4.Background = Brushes.Green;
					break;
			}

			score++;
		}

		private void WrongAns()
		{
			switch (request.answerId)
			{
				case 0:
					BN_Ans1.Background = Brushes.Red;
					BN_Ans2.Background = Brushes.Red;
					BN_Ans3.Background = Brushes.Red;
					BN_Ans4.Background = Brushes.Red;
					break;
				case 1:
					BN_Ans1.Background = Brushes.Red;
					break;
				case 2:
					BN_Ans2.Background = Brushes.Red;
					break;
				case 3:
					BN_Ans3.Background = Brushes.Red;
					break;
				case 4:
					BN_Ans4.Background = Brushes.Red;
					break;
			}
		}

		private void BN_Exit_Click(object sender, RoutedEventArgs e)
		{
			App.com.SendMessageWithoutData((uint)Codes.LeaveGameCode);
			LeaveGameResponse leaveGame = (LeaveGameResponse)App.com.ReceiveMessage();
			if (leaveGame.status == 1)
			{
				App.GoToMenu();
			}
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			BN_Exit.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
			App.ExitGame();
		}
	}
}
