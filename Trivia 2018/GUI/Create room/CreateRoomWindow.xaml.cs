﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClientSolution
{
	/// <summary>
	/// Interaction logic for CreateRoomWindow.xaml
	/// </summary>
	public partial class CreateRoomWindow : Window
	{
		public CreateRoomWindow()
		{
			InitializeComponent();
		}

		private void B_Send(object sender, RoutedEventArgs e)
		{
			CreateRoomRequest newRoom = new CreateRoomRequest
			{
				maxUsers = Convert.ToUInt32(T_NumOfPlayers.Text, 10),
				roomName = T_RoomName.Text,
				questionCount = Convert.ToUInt32(T_NumOfQuestions.Text, 10),
				answerTimeout = Convert.ToUInt32(T_TimePerQuestion.Text, 10)
			};

			App.com.SendMessage<CreateRoomRequest>(ref newRoom, (uint)Codes.CreateRoomCode);
			CreateRoomResponse response = (CreateRoomResponse)App.com.ReceiveMessage();

			if (response.status == 1)
			{
				App.GoToManagerWaitingRoom();
			}
			else
			{
				MessageBox.Show("Error Creating Room");
			}
		}

		private void B_Cancel(object sender, RoutedEventArgs e)
		{
			App.GoToMenu();
		}

		public void Reset()
		{
			T_NumOfPlayers.Text = "";
			T_NumOfQuestions.Text = "";
			T_RoomName.Text = "";
			T_TimePerQuestion.Text = "";
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			App.ExitGame();
		}
	}
}
