﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClientSolution
{
    /// <summary>
    /// Interaction logic for SignupWindow.xaml
    /// </summary>
    public partial class SignupWindow : Window
    {
        public SignupWindow()
        {
            InitializeComponent();
        }

        private void B_signup(object sender, RoutedEventArgs e)
        {
            SignupRequest signupReq;
            SignupResponse signupRes;
            signupReq.username = T_username.Text;
            signupReq.password = T_password.Text;
            signupReq.email = T_email.Text;

            App.com.SendMessage<SignupRequest>(ref signupReq, (uint)Codes.SignupCode);
            signupRes = (SignupResponse)App.com.ReceiveMessage();

            if (signupRes.status == 0)
            {
                MessageBox.Show("Username, Password or Email invalid!", "ERROR");
            }
            else
            {
                App.username = signupReq.username;
                App.GoToMenu();
            }
        }

        private void B_back(object sender, RoutedEventArgs e)
        {
            App.GoToLogin();
        }

		public void Reset()
		{
			T_email.Text = "";
			T_password.Text = "";
			T_username.Text = "";
		}
    }
}
