﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace TriviaClientSolution
{
	/// <summary>
	/// Interaction logic for JoinRoomWindow.xaml
	/// </summary>
	public partial class JoinRoomWindow : Window
	{
		private static List<AvailableRoom> rooms = new List<AvailableRoom>();
		private static List<Participants> participants = new List<Participants>();

		public JoinRoomWindow()
		{
			InitializeComponent();
			L_userList.ItemsSource = rooms;
			L_participantsList.ItemsSource = participants;
		}

		public void UpdateWindow()
		{
			App.com.SendMessageWithoutData((uint)Codes.GetRoomsCode);
			GetRoomsResponse response = (GetRoomsResponse)App.com.ReceiveMessage();
			try
			{
				if (response.status == 1)
				{
					foreach (RoomData room in response.rooms)
					{
						rooms.Add(new AvailableRoom() { Title = room.name, Id = room.id });
					}
				}
			}
			catch (Exception)
			{ }
		}

		private void B_Join(object sender, RoutedEventArgs e)
		{
			try
			{
				AvailableRoom availableRoom = (AvailableRoom)L_userList.SelectedItem;

				JoinRoomRequest request = new JoinRoomRequest()
				{
					roomId = availableRoom.Id
				};

				App.com.SendMessage<JoinRoomRequest>(ref request, (uint)Codes.JoinRoomCode);
				JoinRoomResponse response = (JoinRoomResponse)App.com.ReceiveMessage();
				if (response.status == 1)
				{
					App.GoToWaitingRoom();
				}
			}
			catch (Exception)
			{ }
		}

		private void B_Refresh(object sender, RoutedEventArgs e)
		{
			rooms.Clear();
			participants.Clear();
			UpdateWindow();
			L_userList.ItemsSource = null;
			L_userList.ItemsSource = rooms;
			L_participantsList.ItemsSource = null;
			L_participantsList.ItemsSource = participants;
		}

		private void L_userList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				AvailableRoom availableRoom = (AvailableRoom)e.AddedItems[0];

				GetPlayersInRoomRequest request = new GetPlayersInRoomRequest()
				{
					roomId = availableRoom.Id
				};

				App.com.SendMessage<GetPlayersInRoomRequest>(ref request, (uint)Codes.GetPlayersInRoomCode);
				GetPlayersInRoomResponse response = (GetPlayersInRoomResponse)App.com.ReceiveMessage();

				if (response.status == 1)
				{
					participants.Clear();
					foreach (string name in response.players)
					{
						participants.Add(new Participants() { Name = name });
					}
				}
				
				
				L_participantsList.ItemsSource = null;
				L_participantsList.ItemsSource = participants;
			}
			catch (Exception)
			{
				participants.Clear();
				L_participantsList.ItemsSource = null;
				L_participantsList.ItemsSource = participants;
			}
		}

		private void B_Cancel(object sender, RoutedEventArgs e)
		{
			App.GoToMenu();
		}

		public void Reset()
		{
			rooms.Clear();
			participants.Clear();
			L_userList.ItemsSource = null;
			L_userList.ItemsSource = rooms;
			L_participantsList.ItemsSource = null;
			L_participantsList.ItemsSource = participants;
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			App.ExitGame();
		}
	}

	public class AvailableRoom
	{
		public string Title { get; set; }
		public uint Id { get; set; }
	}
}
