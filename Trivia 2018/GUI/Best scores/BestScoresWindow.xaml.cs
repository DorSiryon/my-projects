﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClientSolution
{
	/// <summary>
	/// Interaction logic for BestScoresWindow.xaml
	/// </summary>
	public partial class BestScoresWindow : Window
	{
		public static List<Scores> scoreList = new List<Scores>();

		public BestScoresWindow()
		{
			InitializeComponent();
		}

		
		public void UpdateWindow()
		{
			scoreList.Clear();
			
			App.com.SendMessageWithoutData((uint)Codes.HighscoreCode);
			HighscoreResponse response = (HighscoreResponse)App.com.ReceiveMessage();
			if (response.status == 1)
			{
				Dictionary<string, uint> order = new Dictionary<string, uint>();
				foreach (Highscore user in response.highscores)
				{
					order.Add(user.username, user.score);
				}
				foreach (KeyValuePair<string, uint> user in order.OrderByDescending(key => key.Value))
				{
					scoreList.Add(new Scores() { Name = user.Key, Score = user.Value });
				}
				
				L_users.ItemsSource = scoreList;
			}
		}

		private void B_Back(object sender, RoutedEventArgs e)
		{
			App.GoToMenu();
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			App.ExitGame();
		}
	}

	public class Scores
	{
		public string Name { get; set; }
		public uint Score { get; set; }
	}
}
