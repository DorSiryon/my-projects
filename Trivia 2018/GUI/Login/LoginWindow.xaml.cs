﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;

namespace TriviaClientSolution
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void B_signup(object sender, RoutedEventArgs e)
        {
            App.GoToSignup();
        }

        private void B_signin(object sender, RoutedEventArgs e)
        {
            LoginRequest loginReq;
            LoginResponse loginRes;
            loginReq.username = T_username.Text;
            loginReq.password = T_password.Text;

            App.com.SendMessage<LoginRequest>(ref loginReq, (uint)Codes.LoginCode);
            loginRes = (LoginResponse)App.com.ReceiveMessage();
            
            if (loginRes.status == 0)
            {
                MessageBox.Show("Either Username or Password is not correct, or user already logged in", "ERROR");
            }
            else
            {
                App.username = loginReq.username;
                App.GoToMenu();
            }
			
		}

		public void Reset()
		{
			T_password.Text = "";
			T_username.Text = "";
		}
	}
}
