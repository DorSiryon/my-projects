﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using Newtonsoft.Json;

namespace TriviaClientSolution
{
    public class Communicator
    {
        private TcpClient client;
        private NetworkStream clientStream;

        public void EstablishConnection()
        {
            bool retry;
            do
            {
                try
                {
                    client = new TcpClient();
                    IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 498);
                    client.Connect(serverEndPoint);
                    clientStream = client.GetStream();
                    retry = false;
                }
                catch
                {
                    retry = App.RetryConnection();
                    if (!retry)
                    {
                        Environment.Exit(0);
                    }
                }
            } while (retry);
        }

        public void SendMessage<ReqType>(ref ReqType request, uint id)
        {
            string reqOut = Serializer.SerializeReqeust(ref request, id);
            byte[] buffer = new ASCIIEncoding().GetBytes(reqOut);
            clientStream.Write(buffer, 0, buffer.Length);
            clientStream.Flush();
        }

		public void SendMessageWithoutData(uint id)
		{
			string reqOut = Serializer.SerializeReqeusts(id);
			byte[] buffer = new ASCIIEncoding().GetBytes(reqOut);
			clientStream.Write(buffer, 0, buffer.Length);
			clientStream.Flush();
		}

		public Response ReceiveMessage()
        {
            uint id = GetAsciiPartFromServer();
            uint size = GetSizePartFromServer();
            string msg = GetMessagePartFromServer(size);
            Response response = Deserializer.DeserializeResponse(msg, id);
            return response;
        }

        public uint GetAsciiPartFromServer()
        {
            string buffer = GetMessagePartFromServer(1);
            return (uint)buffer[0];
        }

        public uint GetSizePartFromServer()
        {
            string buffer = GetMessagePartFromServer(4);
            return uint.Parse(buffer);
        }

        public string GetMessagePartFromServer(uint size)
        {
            byte[] buffer = new byte[size];
            clientStream.Read(buffer, 0, buffer.Length);
            return System.Text.Encoding.UTF8.GetString(buffer);
        }


    }
}
