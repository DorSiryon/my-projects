﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Threading;

namespace TriviaClientSolution
{
    /// <summary>
    /// Interaction logic for ManagerWaitingRoomWindow.xaml
    /// </summary>
    public partial class ManagerWaitingRoomWindow : Window
    {
		private static BackgroundWorker listUpdater = new BackgroundWorker();
		private static List<Participants> participantsList = new List<Participants>();

		public ManagerWaitingRoomWindow()
        {
            InitializeComponent();
			L_users.ItemsSource = participantsList;
			listUpdater.WorkerReportsProgress = true;
			listUpdater.WorkerSupportsCancellation = true;
			listUpdater.DoWork += GetUpdate;
			listUpdater.ProgressChanged += UpdateList;
		}

		public void UpdateWindow()
		{
			App.com.SendMessageWithoutData((uint)Codes.GetRoomStateCode);
			GetRoomStateResponse response = (GetRoomStateResponse)App.com.ReceiveMessage();
			if (response.status == 1)
			{
				TB_maxPlayers.Text = response.maxPlayers.ToString();
				TB_questionNum.Text = response.questionCount.ToString();
				TB_RoomName.Text = response.roomName;
				TB_timePerQuestion.Text = response.answerTimeout.ToString();
			}

			foreach (string name in response.players)
			{
				participantsList.Add(new Participants() { Name = name });
			}

			listUpdater.RunWorkerAsync();
		}

		private void GetUpdate(object sender, DoWorkEventArgs e) //do work
		{
			GetRoomStateResponse response;
			while (!listUpdater.CancellationPending)
			{
				App.com.SendMessageWithoutData((uint)Codes.GetRoomStateCode);
				response = (GetRoomStateResponse)App.com.ReceiveMessage();

				if (response.status == 0 || response.hasGameBegun == 1)
				{
					e.Cancel = true;
					return;
				}
				else if (response.players.Count != L_users.Items.Count)
				{
					participantsList.Clear();
					foreach (string name in response.players)
					{
						participantsList.Add(new Participants() { Name = name });
					}
					listUpdater.ReportProgress(1);
				}
				Thread.Sleep(200);
			}
			e.Cancel = true;
		}

		private void UpdateList(object sender, ProgressChangedEventArgs e) //progress changed
		{
			L_users.ItemsSource = null;
			L_users.ItemsSource = participantsList;
		}

		private void B_Start(object sender, RoutedEventArgs e)
		{
			listUpdater.CancelAsync();

			App.com.SendMessageWithoutData((uint)Codes.GetRoomStateCode);
			GetRoomStateResponse roomState = (GetRoomStateResponse)App.com.ReceiveMessage();
			if (roomState.status == 1)
			{
				App.gameWnd.roomName = roomState.roomName;
				App.gameWnd.questionCount = roomState.questionCount;
				App.gameWnd.time = (int)roomState.answerTimeout;
			}

			App.com.SendMessageWithoutData((uint)Codes.StartGameCode);
			StartGameResponse startGame = (StartGameResponse)App.com.ReceiveMessage();
			if (startGame.status == 1)
			{
				App.GoToGame();
			}
			else
			{
				MessageBox.Show("Couldn't start the game");
			}
		}

		private void B_Close(object sender, RoutedEventArgs e)
		{
			listUpdater.CancelAsync();
			App.com.SendMessageWithoutData((uint)Codes.CloseRoomCode);
			CloseRoomResponse response = (CloseRoomResponse)App.com.ReceiveMessage();
			if (response.status == 1)
			{
				App.GoToMenu();
			}
			else
			{
				MessageBox.Show("Couldn't close the room");
			}
		}

		public void Reset()
		{
			participantsList.Clear();
			L_users.ItemsSource = null;
			L_users.ItemsSource = participantsList;
			TB_maxPlayers.Text = "";
			TB_questionNum.Text = "";
			TB_RoomName.Text = "";
			TB_timePerQuestion.Text = "";
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			BN_Close.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
			App.ExitGame();
		}
	}

	public class Participants
	{
		public string Name { get; set; }
	}
}
