﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TriviaClientSolution
{
    enum Codes
    {
        ErrorCode = 0,
        SignupCode,
        LoginCode,

        LogoutCode,
        GetRoomsCode,
        GetPlayersInRoomCode,
        JoinRoomCode,
        CreateRoomCode,
        HighscoreCode,
		UserStatusCode,

		CloseRoomCode,
        StartGameCode,
        GetRoomStateCode,
        LeaveRoomCode,

		GetQuestionCode,
		SubmitAnswerCode,
		GetGameResultsCode,
		LeaveGameCode
	};

    public struct LoginRequest
    {
        public string username;
        public string password;
    };

    public struct SignupRequest
    {        
        public string username;
        public string password;
        public string email;
    };

    public struct GetPlayersInRoomRequest
    {        
        public uint roomId;
    };

    public struct JoinRoomRequest
    {
        public uint roomId;
    };

    public struct CreateRoomRequest
    {    
        public string roomName;
        public uint maxUsers;
        public uint questionCount;
        public uint answerTimeout;
    };

	public struct SubmitAnswerRequest
	{
		public uint answerId;
		public uint answerTime;
	};

	public class Serializer
    {
        public static string SerializeReqeust<ReqType>(ref ReqType request, uint id)
        {
            char code = (char)id;
            string output = code.ToString();
            string msg = JsonConvert.SerializeObject(request);
            output += msg.Length.ToString().PadLeft(4, '0');
            output += msg;
            return output;
        }

		public static string SerializeReqeusts(uint id)
		{
			char code = (char)id;
			string output = code.ToString();
			string msg = "";
			output += msg.Length.ToString().PadLeft(4, '0');
			output += msg;
			return output;
		}
	}
}
