﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TriviaClientSolution
{
    /// <summary>
    /// Interaction logic for MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Window
    {
        public MenuWindow()
        {
            InitializeComponent();
        }

        public void UpdateWindow()
        {
            TB_helloUser.Text = "Hello " + App.username + "!";
        }

		private void B_Quit(object sender, RoutedEventArgs e)
		{
			App.ExitGame();
		}

		private void B_SignOut(object sender, RoutedEventArgs e)
		{
			LogoutResponse response = App.Logout();
			if (response.status == 1)
			{
				App.GoToLogin();
			}
		}

		private void B_BestScores(object sender, RoutedEventArgs e)
		{
			App.GoToBestScores();
		}

		private void B_MyStatus(object sender, RoutedEventArgs e)
		{
			App.GoToMyStatus();
		}

		private void B_CreateRoom(object sender, RoutedEventArgs e)
		{
			App.GoToCreateRoom();
		}

		private void B_JoinRoom(object sender, RoutedEventArgs e)
		{
			App.GoToJoinRoom();
		}

		private void Window_Closed(object sender, EventArgs e)
		{
			App.ExitGame();
		}
	}
}
