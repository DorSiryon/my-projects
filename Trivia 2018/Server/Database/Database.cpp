#include "Database.h"
#include <string>

std::mutex mtx_database;

IDatabase::~IDatabase()
{

}

/******************************************* sqliteDatabase *******************************************/

bool sqliteDatabase::execQuery(std::string sqlStatement)
{
	char* errMessage = nullptr;

	std::unique_lock<std::mutex> databaseLocker(mtx_database);
	
	int res = sqlite3_exec(_database, sqlStatement.c_str(), nullptr, nullptr, &errMessage);
	
	databaseLocker.unlock();

	if (res != SQLITE_OK)
	{
		std::cout << __FUNCTION__ << " - " << errMessage << std::endl;
		std::cout << sqlStatement << std::endl << std::endl;
		return false;
	}

	return true;
}

bool sqliteDatabase::execQuery(std::string sqlStatement, int(*callback)(void*, int, char **, char **), void* container)
{
	char* errMessage = nullptr;
	
	std::unique_lock<std::mutex> databaseLocker(mtx_database);
	
	int res = sqlite3_exec(_database, sqlStatement.c_str(), callback, container, &errMessage);

	databaseLocker.unlock();

	if (res != SQLITE_OK)
	{
		std::cout << __FUNCTION__ << " - " << errMessage << std::endl;
		std::cout << sqlStatement << std::endl << std::endl;
		return false;
	}

	return true;
}

bool sqliteDatabase::open()
{
	int doesFileExist = _access(TRIVIA_DB, 0);
	int res = sqlite3_open(TRIVIA_DB, &_database);

	if (res != SQLITE_OK)
	{
		_database = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}

	if (doesFileExist == -1)
	{
		execQuery("BEGIN TRANSACTION;");

		try
		{
			//table USERS
			execQuery("CREATE TABLE IF NOT EXISTS `USERS` ( "
				"`USERNAME`		TEXT NOT NULL PRIMARY KEY, "
				"`PASSWORD`		TEXT NOT NULL, "
				"`EMAIL`		TEXT NOT NULL);");

			//table QUESTIONS
			execQuery("CREATE TABLE IF NOT EXISTS `QUESTIONS` ( "
				"`QUESTION_ID`		INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
				"`QUESTION`			TEXT NOT NULL, "
				"`CORRECT_ANS`		TEXT NOT NULL, "
				"`ANS2`				TEXT NOT NULL, "
				"`ANS3`				TEXT NOT NULL, "
				"`ANS4`				TEXT NOT NULL);");

			//table GAMES
			execQuery("CREATE TABLE IF NOT EXISTS `GAMES` ( "
				"`GAME_ID`			INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
				"`START_TIME`		DATETIME NOT NULL, "
				"`END_TIME`			DATETIME);");

			//table ANSWERS
			execQuery("CREATE TABLE IF NOT EXISTS `ANSWERS` ( "
				"`GAME_ID`			INTEGER NOT NULL, "
				"'USERNAME'			TEXT NOT NULL, "
				"`QUESTION_ID`		INTEGER NOT NULL, "
				"`ANSWER`			TEXT NOT NULL, "
				"`IS_CORRECT`		INTEGER NOT NULL, "
				"`ANSWER_TIME`		INTEGER NOT NULL, "

				"PRIMARY KEY(`GAME_ID`,`USERNAME`,`QUESTION_ID`), "
				"FOREIGN KEY(`GAME_ID`) REFERENCES `GAMES`(`GAME_ID`), "
				"FOREIGN KEY(`USERNAME`) REFERENCES `USERS`(`USERNAME`), "
				"FOREIGN KEY(`QUESTION_ID`) REFERENCES `QUESTIONS`(`QUESTION_ID`));");
			
			setQuestions();

			execQuery("COMMIT;");
		}
		catch (std::exception& error)
		{
			std::cout << error.what() << std::endl;
			execQuery("ROLLBACK;");
		}
	}

	return true;
}

bool sqliteDatabase::doesUserExist(std::string username)
{
	bool doesExist = false;

	execQuery("SELECT * FROM USERS "
		"WHERE USERNAME = '" + username + "';", doesExistHandler, &doesExist);

	return doesExist;
}

void sqliteDatabase::addUser(std::string username, std::string password, std::string email)
{
	execQuery("INSERT INTO USERS "
		"VALUES('" + username + "', '" + password + "', '" + email + "');");
}

bool sqliteDatabase::isPasswordCorrect(std::string username, std::string password)
{
	std::string userPassword;

	execQuery("SELECT PASSWORD FROM USERS "
		"WHERE USERNAME = '" + username + "';", passwordHandler, &userPassword);

	return (userPassword == password);
}

unsigned int sqliteDatabase::addGame()
{
	unsigned int gameId = 0;
	
	execQuery("INSERT INTO GAMES(START_TIME) "
		"VALUES(DATETIME('now', 'localtime'));");

	execQuery("SELECT GAME_ID FROM GAMES "
		"ORDER BY GAME_ID DESC "
		"LIMIT 1;", gameIdHandler, &gameId);

	return gameId;
}

void sqliteDatabase::updateFinishedGame(unsigned int gameId)
{
	execQuery("UPDATE GAMES "
		"SET END_TIME = DATETIME('now', 'localtime') "
		"WHERE GAME_ID = " + std::to_string(gameId) + ";");
}

void sqliteDatabase::setQuestions()
{
	//question 1
	addQuestion("What team won the very first NBA game?",
				"New York Knicks", "Philadelphia Warriors", "Toronto Huskies", "Chicago Stags");

	//question 2
	addQuestion("What NBA player scored 100 points on March 2, 1962?",
		"Wilt Chamberlain", "Bill Russell", "Kareem Abdul-Jabbar", "Elgin Baylor");

	//question 3
	addQuestion("Who was the first player in NBA history to be elected league MVP by a unanimous vote?",
		"Stephen Curry", "Magic Johnson", "Michael Jordan", "Lebron James");

	//question 4
	addQuestion("What new kind of shot did Joe Fulks score a record 63 points with in one game in 1949?",
		"Jump Shot", "Hook Shot", "Three-Point Shot", "Free Throw");

	//question 5
	addQuestion("Who scored the first three-point basket in NBA history?",
		"Chris Ford", "Wes Unseld", "Gene Stump", "Larry Bird");

	//question 6
	addQuestion("Who was the youngest player to score 10,000 points in the NBA?",
		"Lebron James", "Michael Jordan", "Kobe Bryant", "Wilt Chamberlain");

	//question 7
	addQuestion("What team owns the longest winning streak in NBA history?",
		"Miami Heat", "Chicago Bulls", "Los Angeles Lakers", "Golden State Warriors");

	//question 8
	addQuestion("What player won All-Star Game MVP, NBA MVP, and NBA Finals MVP awards in 2000?",
		"Shaquille Oneal", "Michael Jordan", "Kobe Bryant", "Tim Duncan");
	
	//question 9
	addQuestion("What team did Wilt Chamberlain finish his NBA career with?",
		"Los Angeles Lakers", "Dallas Mavericks", "Philadelphia 76ers", "Chicago Bulls");

	//question 10
	addQuestion("What team drafted Pau Gasol in the 2001 NBA draft?",
		"Atlanta Hawks", "Memphis Grizzlies", "Los Angeles Lakers", "San Antonio Spurs");

	//question 11
	addQuestion("What NBA player has won the most league MVPs?",
		"Kareem Abdul-Jabbar", "Lebron James", "Stephen Curry", "Michael Jordan");

	//question 12
	addQuestion("What teammates were nicknamed the Splash Brothers?",
		"Stephen Curry & Klay Thompson", "Michael Jordan & Scottie Pippen", "Kevin Durant & Russell Westbrook", "Jerry West & Wilt Chamberlain");

	//question 13
	addQuestion("How many points did LeBron James score in his first NBA game?",
		"25", "61", "19", "41");

	//question 14
	addQuestion("Who is the NBA Championship trophy named after?",
		"Larry Obrien", "Red Auerbach", "James Naismith", "Maurice Podoloff");

	//question 15
	addQuestion("Who has the most blocked shots in NBA history?",
		"Hakeem Olajuwon", "Mark Eaton", "Dikembe Mutombo", "Kareem Abdul-Jabbar");

	//question 16
	addQuestion("Who was the first player to lead the NBA in both scoring and steals in the same season?",
		"Michael Jordan", "Bob Cousy", "John Stockton", "Kobe Bryant");

	//question 17
	addQuestion("In what city were the Houston Rockets originally founded?",
		"San Diego", "Austin", "Houston", "Los Angeles");

	//question 18
	addQuestion("What player holds the NBA record for most career assists?",
		"John Stockton", "Jason Kidd", "Magic Johnson", "Steve Nash");

	//question 19
	addQuestion("What player holds the record for most blocked shots in a game?",
		"Elmore Smith", "Manute Bol", "Shaquille Oneal", "Mark Eaton");

	//question 20
	addQuestion("Who was the first NBA player to record a triple-double in the All-Star Game?",
		"Michael Jordan", "Lebron James", "Dwyane Wade", "John Stockton");

	//question 21
	addQuestion("What NBA team won eight consecutive championships from 1959 to 1966?",
		"Boston Celtics", "New York Knicks", "Philadelphia Warriors", "Minneapolis Lakers");
}

void sqliteDatabase::addQuestion(std::string question, std::string correctAns, std::string ans2, std::string ans3, std::string ans4)
{
	execQuery("INSERT INTO QUESTIONS(QUESTION, CORRECT_ANS, ANS2, ANS3, ANS4) "
		"VALUES('" + question + "', '" + correctAns + "', '" + ans2 + "', '" + ans3 + "', '" + ans4 + "');");
}

std::vector<Question*> sqliteDatabase::getQuestions(int numQuestionsToGet)
{
	std::vector<Question*> questionsList;
	
	execQuery("SELECT * FROM QUESTIONS "
		"ORDER BY RANDOM() "
		"LIMIT " + std::to_string(numQuestionsToGet) + ";", questionsHandler, &questionsList);

	return questionsList;
}

void sqliteDatabase::addAnswer(unsigned int gameId,
							std::string username,
							unsigned int questionId,
							std::string answer,
							unsigned int isCorrect,
							unsigned int answerTime)
{
	execQuery("INSERT INTO ANSWERS "
		"VALUES(" + std::to_string(gameId) + ", '"
				+ username + "', " 
				+ std::to_string(questionId) + ", '" 
				+ answer + "', " 
				+ std::to_string(isCorrect) + ", " 
				+ std::to_string(answerTime) + ");");
}

PlayerResults sqliteDatabase::getUserStatisticss(std::string username)
{
	PlayerResults userStats;
	bool doesExist = false;
	
	execQuery("SELECT * FROM ANSWERS "
		"WHERE USERNAME = '" + username + "';", doesExistHandler, &doesExist);
	
	userStats.username = username;
	userStats.gamesNum = 0;
	userStats.correctAnswerCount = 0;
	userStats.wrongAnswerCount = 0;
	userStats.averageAnswerTime = 0;

	if (doesExist)
	{
		userStats.gamesNum = getGamesNumOfUser(username);
		userStats.correctAnswerCount = getCorrectAnsCount(username);
		userStats.wrongAnswerCount = getWrongAnsCount(username);
		userStats.averageAnswerTime = (unsigned int)getAvgAnsTime(username);
	}

	return userStats;
}

int sqliteDatabase::getGamesNumOfUser(std::string username)
{
	int gamesNum = 0;

	execQuery("SELECT DISTINCT GAME_ID, USERNAME FROM ANSWERS "
		"WHERE USERNAME = '" + username + "';", rowsCountHandler, &gamesNum);

	return gamesNum;
}

int sqliteDatabase::getCorrectAnsCount(std::string username)
{
	int correctAnsCount = 0;

	execQuery("SELECT COUNT(*) FROM ANSWERS "
		"WHERE USERNAME = '" + username + "' AND IS_CORRECT = 1;", countHandler, &correctAnsCount);

	return correctAnsCount;
}

int sqliteDatabase::getWrongAnsCount(std::string username)
{
	int wrongAnsCount = 0;

	execQuery("SELECT COUNT(*) FROM ANSWERS "
		"WHERE USERNAME = '" + username + "' AND IS_CORRECT = 0;", countHandler, &wrongAnsCount);

	return wrongAnsCount;
}

float sqliteDatabase::getAvgAnsTime(std::string username)
{
	float avgAnsTime = 0;

	execQuery("SELECT AVG(ANSWER_TIME) FROM ANSWERS "
		"WHERE USERNAME = '" + username + "';", avgAnsTimeHandler, &avgAnsTime);

	return avgAnsTime;
}

std::map<std::string, int> sqliteDatabase::getHighscores()
{
	std::map<std::string, int> highscores;

	execQuery("SELECT USERNAME, COUNT(IS_CORRECT) FROM ANSWERS "
		"WHERE IS_CORRECT = 1 "
		"GROUP BY USERNAME "
		"ORDER BY COUNT(IS_CORRECT) DESC "
		"LIMIT 3;", highscoresHandler, &highscores);

	//if highscores list is not full
	if (highscores.size() < 3)
	{
		execQuery("SELECT USERNAME FROM USERS;", fillHighscoresHandler, &highscores);
	}

	return highscores;
}

/******************************************* CALLBACKS *******************************************/

int doesExistHandler(void* doesExist, int argc, char** argv, char** azColName)
{
	bool* casted_doesExist = (bool*)doesExist;

	*casted_doesExist = true;

	return 0;
}

int rowsCountHandler(void* rowsCounter, int argc, char** argv, char** azColName)
{
	int* casted_rowsCounter = (int*)rowsCounter;
	
	(*casted_rowsCounter)++;

	return 0;
}

int passwordHandler(void* password, int argc, char** argv, char** azColName)
{
	std::string* casted_password = (std::string*)password;

	if (std::string(azColName[0]) == "PASSWORD")
	{
		*casted_password = std::string(argv[0]);
	}

	return 0;
}

/**
*	CALLBACK FUNCTION:
*gets the SQL COUNT() function value.
**Must* use COUNT(*) and *not* COUNT(value)
*
*1st arg input:
*	int*
*/
int countHandler(void* countNum, int argc, char** argv, char** azColName)
{
	int* casted_countNum = (int*)countNum;

	if (std::string(azColName[0]) == "COUNT(*)")
	{
		if (argv[0] == nullptr)
		{
			*casted_countNum = 0;
		}
		else
		{
			*casted_countNum = atoi(argv[0]);
		}
	}

	return 0;
}

/**
*	CALLBACK FUNCTION:
*
*
*1st arg input:
*	float*
*/
int avgAnsTimeHandler(void* avgNum, int argc, char** argv, char** azColName)
{
	float* casted_avgNum = (float*)avgNum;

	if (std::string(azColName[0]) == "AVG(ANSWER_TIME)")
	{
		if (argv[0] == nullptr)
		{
			*casted_avgNum = 0;
		}
		else
		{
			*casted_avgNum = std::stof(argv[0]);
		}
	}

	return 0;
}

int questionsHandler(void* questionsList, int argc, char** argv, char** azColName)
{
	int i = 0;
	Question* question = new Question();

	std::vector<Question*>* casted_questionsList = (std::vector<Question*>*)questionsList;

	for (i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "QUESTION_ID")
		{
			question->setQuestionId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "QUESTION")
		{
			question->setQuestion(argv[i]);
		}
		else if (std::string(azColName[i]) == "CORRECT_ANS")
		{
			question->addCorrectAnswer(argv[i]);
		}
		else if (std::string(azColName[i]).find("ANS") != std::string::npos)
		{
			question->addAnswer(argv[i]);
		}
	}

	casted_questionsList->push_back(question);
	return 0;
}

int highscoresHandler(void* highscores, int argc, char** argv, char** azColName)
{
	int i = 0;
	std::pair<std::string, int> highscore;

	std::map<std::string, int>* casted_highscores = (std::map<std::string, int>*)highscores;

	for (i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "USERNAME")
		{
			highscore.first = argv[i];
		}
		else if (std::string(azColName[i]) == "COUNT(IS_CORRECT)")
		{
			highscore.second = atoi(argv[i]);
		}
	}

	casted_highscores->insert(highscore);
	return 0;
}

int fillHighscoresHandler(void* highscores, int argc, char** argv, char** azColName)
{
	std::map<std::string, int>* casted_highscores = (std::map<std::string, int>*)highscores;
	std::pair<std::string, int> highscore;

	if (casted_highscores->size() < 3)
	{
		if (std::string(azColName[0]) == "USERNAME")
		{
			if ((*casted_highscores).find(std::string(argv[0])) == (*casted_highscores).end())
			{
				highscore.first = argv[0];
				highscore.second = 0;
				casted_highscores->insert(highscore);
			}
		}
	}

	return 0;
}

int gameIdHandler(void* gameId, int argc, char** argv, char** azColName)
{
	unsigned int* casted_gameId = (unsigned int*)gameId;

	if (std::string(azColName[0]) == "GAME_ID")
	{
		*casted_gameId = atoi(argv[0]);
	}

	return 0;
}
