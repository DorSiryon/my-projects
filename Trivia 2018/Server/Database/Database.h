#pragma once

#include <iostream>
#include <map>
#include <list>
#include <io.h>
#include <cstdlib>
#include <ctime>
#include "sqlite3.h"
#include "Response.h"
#include "Managers.h"
#include <mutex>

#define TRIVIA_DB "TriviaDB.sqlite"
#define MAX_NUM_OF_QUESTIONS 21

struct PlayerResults;
class LoggedUser;
class Question;

// CALLBACK Functions
int doesExistHandler(void* doesExist, int argc, char** argv, char** azColName);
int rowsCountHandler(void* rowsCounter, int argc, char** argv, char** azColName);
int passwordHandler(void* password, int argc, char** argv, char** azColName);
int countHandler(void* countNum, int argc, char** argv, char** azColName);
int avgAnsTimeHandler(void* avgNum, int argc, char** argv, char** azColName);
int questionsHandler(void* questionsList, int argc, char** argv, char** azColName);
int highscoresHandler(void* highscores, int argc, char** argv, char** azColName);
int fillHighscoresHandler(void* highscores, int argc, char** argv, char** azColName);
int gameIdHandler(void* gameId, int argc, char** argv, char** azColName);

class IDatabase
{
public:
	virtual ~IDatabase() = 0;

	//user:
	virtual void addUser(std::string username, std::string password, std::string email) = 0;
	virtual bool doesUserExist(std::string username) = 0;
	virtual bool isPasswordCorrect(std::string username, std::string password) = 0;
	
	virtual unsigned int addGame() = 0;
	virtual void updateFinishedGame(unsigned int gameId) = 0;

	virtual void setQuestions() = 0;
	virtual void addQuestion(std::string question, std::string correctAns, std::string ans2, std::string ans3, std::string ans4) = 0;
	virtual std::vector<Question*> getQuestions(int numQuestionsToGet) = 0;

	virtual void addAnswer(unsigned int gameId,
		std::string username,
		unsigned int questionId,
		std::string answer,
		unsigned int isCorrect,
		unsigned int answerTime) = 0;

	virtual PlayerResults getUserStatisticss(std::string username) = 0;
	virtual int getGamesNumOfUser(std::string username) = 0;
	virtual int getCorrectAnsCount(std::string username) = 0;
	virtual int getWrongAnsCount(std::string username) = 0;
	virtual float getAvgAnsTime(std::string username) = 0;

	virtual std::map<std::string, int> getHighscores() = 0;

	virtual bool open() = 0;
};

class sqliteDatabase : public IDatabase
{
public:
	sqliteDatabase() = default;
	~sqliteDatabase() {};
	
	//user:
	void addUser(std::string username, std::string password, std::string email);
	bool doesUserExist(std::string);
	bool isPasswordCorrect(std::string username, std::string password);

	unsigned int addGame();
	void updateFinishedGame(unsigned int gameId);
	
	void setQuestions();
	void addQuestion(std::string question, std::string correctAns, std::string ans2, std::string ans3, std::string ans4);
	std::vector<Question*> getQuestions(int numQuestionsToGet);
	
	void addAnswer(unsigned int gameId,
				std::string username, 
				unsigned int questionId, 
				std::string answer,	
				unsigned int isCorrect, 
				unsigned int answerTime);

	PlayerResults getUserStatisticss(std::string username);
	int getGamesNumOfUser(std::string username);
	int getCorrectAnsCount(std::string username);
	int getWrongAnsCount(std::string username);
	float getAvgAnsTime(std::string username);

	std::map<std::string, int> getHighscores();

	bool open();

private:
	sqlite3* _database;

	bool execQuery(std::string sqlStatement);
	bool execQuery(std::string sqlStatement, int(*callback)(void*, int, char **, char **), void* container);
};
