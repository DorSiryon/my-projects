#pragma once

#include <iostream>
#include <vector>
#include <string>
#include "Helper.h"
#include "Managers.h"
#include <map>


struct RoomData;
struct Highscore;

struct PlayerResults
{
	std::string username;
	unsigned int gamesNum;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averageAnswerTime;
};

struct LoginResponse
{
	unsigned int status;
};

struct SignupResponse
{
	unsigned int status;
};

struct ErrorResponse
{
	std::string message;
};

struct LogoutResponse
{
	unsigned int status;
};

struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
};

struct GetPlayersInRoomResponse
{
	unsigned int status;
	std::vector<std::string> players;
};

struct HighscoreResponse
{
	unsigned int status;
	std::vector<Highscore> highscores;
};

struct UserStatusResponse
{
	unsigned int status;
	PlayerResults results;
};

struct JoinRoomResponse
{
	unsigned int status;
};

struct CreateRoomResponse
{
	unsigned int status;
};

struct CloseRoomResponse
{
	unsigned int status;
};

struct StartGameResponse
{
	unsigned int status;
};

struct GetRoomStateResponse
{
	unsigned int status;
	std::string roomName;
	unsigned int hasGameBegun;
	std::vector<std::string> players;
	unsigned int maxPlayers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct LeaveRoomResponse
{
	unsigned int status;
};

struct GetQuestionResponse
{
	unsigned int status;
	std::string question;
	std::map<unsigned int, std::string> answers;
};

struct SubmitAnswerResponse
{
	unsigned int status;
	unsigned int correctAnswerId;
};

struct GetGameResultsResponse
{
	unsigned int status;
	std::map<std::string, unsigned int> results;
};

struct LeaveGameResponse
{
	unsigned int status;
};

class JsonResponsePacketSerializer
{
public:
	static std::vector<Byte> serializeResponse(ErrorResponse errorRes);
	static std::vector<Byte> serializeResponse(LoginResponse loginRes);
	static std::vector<Byte> serializeResponse(SignupResponse signupRes);

	static std::vector<Byte> serializeResponse(LogoutResponse logoutRes);
	static std::vector<Byte> serializeResponse(GetRoomsResponse getRoomsRes);
	static std::vector<Byte> serializeResponse(GetPlayersInRoomResponse getPlayersRoomRes);
	static std::vector<Byte> serializeResponse(JoinRoomResponse joinRoomRes);
	static std::vector<Byte> serializeResponse(CreateRoomResponse createRoomRes);
	static std::vector<Byte> serializeResponse(HighscoreResponse highscoreRes);
	static std::vector<Byte> serializeResponse(UserStatusResponse userStatusRes);

	static std::vector<Byte> serializeResponse(CloseRoomResponse closeRoomRes);
	static std::vector<Byte> serializeResponse(StartGameResponse startGameRes);
	static std::vector<Byte> serializeResponse(GetRoomStateResponse getRoomStateRes);
	static std::vector<Byte> serializeResponse(LeaveRoomResponse leaveRoomRes);

	static std::vector<Byte> serializeResponse(GetQuestionResponse getQuestionRes);
	static std::vector<Byte> serializeResponse(SubmitAnswerResponse submitAnswerRes);
	static std::vector<Byte> serializeResponse(GetGameResultsResponse getGameResultsRes);
	static std::vector<Byte> serializeResponse(LeaveGameResponse leaveGameRes);
};