#include "Response.h"

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(ErrorResponse errorRes)
{
	std::vector<Byte> buffer;

	json j;
	j["message"] = errorRes.message;

	buffer.push_back(Byte(ErrorCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(LoginResponse loginRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = loginRes.status;

	buffer.push_back(Byte(LoginCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;

}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(SignupResponse signupRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = signupRes.status;

	buffer.push_back(Byte(SignupCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(LogoutResponse logoutRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = logoutRes.status;

	buffer.push_back(Byte(LogoutCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse getRoomsRes)
{
	std::vector<Byte> buffer;
	
	json j;
	j["status"] = getRoomsRes.status;
	j["rooms"];

	std::vector<RoomData>::iterator it;
	for (it = (getRoomsRes.rooms).begin(); it != (getRoomsRes.rooms).end(); ++it)
	{
		j["rooms"].push_back({
			{ "id", it->id },
			{ "hasGameBegun", it->hasGameBegun },
			{ "maxPlayer",it->maxPlayer },
			{ "name", it->name },
			{ "timePerQuestion", it->timePerQuestion }});
	}

	buffer.push_back(Byte(GetRoomsCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse getPlayersRoomRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = getPlayersRoomRes.status;
	j["players"];

	std::vector<std::string>::iterator it;
	for (it = (getPlayersRoomRes.players).begin(); it != (getPlayersRoomRes.players).end(); ++it)
	{
		j["players"].push_back(*it);
	}

	buffer.push_back(Byte(GetPlayersInRoomCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse joinRoomRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = joinRoomRes.status;

	buffer.push_back(Byte(JoinRoomCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse createRoomRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = createRoomRes.status;

	buffer.push_back(Byte(CreateRoomCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(HighscoreResponse highscoreRes)
{
	std::vector<Byte> buffer;
	
	json j;
	j["status"] = highscoreRes.status;
	j["highscores"];

	std::vector<Highscore>::iterator it;
	for (it = (highscoreRes.highscores).begin(); it != (highscoreRes.highscores).end(); ++it)
	{
		j["highscores"].push_back({
			{ "username", it->username },
			{ "score", it->score }});
	}

	buffer.push_back(Byte(HighscoreCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(UserStatusResponse userStatusRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = userStatusRes.status;
	
	j["results"];
	j["results"]["averageAnswerTime"] = userStatusRes.results.averageAnswerTime;
	j["results"]["gamesNum"] = userStatusRes.results.gamesNum;
	j["results"]["correctAnswerCount"] = userStatusRes.results.correctAnswerCount;
	j["results"]["wrongAnswerCount"] = userStatusRes.results.wrongAnswerCount;

	buffer.push_back(Byte(UserStatusCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse closeRoomRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = closeRoomRes.status;

	buffer.push_back(Byte(CloseRoomCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(StartGameResponse startGameRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = startGameRes.status;

	buffer.push_back(Byte(StartGameCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse getRoomStateRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = getRoomStateRes.status;
	j["roomName"] = getRoomStateRes.roomName;
	j["hasGameBegun"] = getRoomStateRes.hasGameBegun;
	j["players"];

	std::vector<std::string>::iterator it;
	for (it = (getRoomStateRes.players).begin(); it != (getRoomStateRes.players).end(); ++it)
	{
		j["players"].push_back(*it);
	}

	j["maxPlayers"] = getRoomStateRes.maxPlayers;
	j["questionCount"] = getRoomStateRes.questionCount;
	j["answerTimeout"] = getRoomStateRes.answerTimeout;

	buffer.push_back(Byte(GetRoomStateCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse leaveRoomRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = leaveRoomRes.status;

	buffer.push_back(Byte(LeaveRoomCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse getQuestionRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = getQuestionRes.status;
	j["question"] = getQuestionRes.question;
	j["answers"];

	std::map<unsigned int, std::string>::iterator it;
	for (it = (getQuestionRes.answers).begin(); it != (getQuestionRes.answers).end(); ++it)
	{
		j["answers"][std::to_string(it->first)] = it->second;
	}

	buffer.push_back(Byte(GetQuestionCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse submitAnswerRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = submitAnswerRes.status;
	j["correctAnswerId"] = submitAnswerRes.correctAnswerId;

	buffer.push_back(Byte(SubmitAnswerCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse getGameResultsRes)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = getGameResultsRes.status;
	
	std::map<std::string, unsigned int>::iterator it;
	for (it = (getGameResultsRes.results).begin(); it != (getGameResultsRes.results).end(); ++it)
	{
		j["results"][it->first] = it->second;
	}

	buffer.push_back(Byte(GetGameResultsCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}

std::vector<Byte> JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse leaveGameResponse)
{
	std::vector<Byte> buffer;

	json j;
	j["status"] = leaveGameResponse.status;

	buffer.push_back(Byte(LeaveGameCode));

	std::string jsonMsgString = j.dump();
	std::string msgSize = getPaddedNumber(jsonMsgString.size(), DATA_SIZE_BYTES_NUM);
	std::copy(msgSize.begin(), msgSize.end(), std::back_inserter(buffer));

	std::copy(jsonMsgString.begin(), jsonMsgString.end(), std::back_inserter(buffer));

	return buffer;
}