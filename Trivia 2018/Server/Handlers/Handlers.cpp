#include "Handlers.h"

IRequestHandler::~IRequestHandler()
{

}

/******************************************* LoginRequestHandler *******************************************/

LoginRequestHandler::LoginRequestHandler(LoginManager* loginManager, RequestHandlerFactory& handlerFactory)
{
	m_loginManager = loginManager;
	m_handlerFactory = &handlerFactory;
}

LoginRequestHandler::~LoginRequestHandler()
{

}

bool LoginRequestHandler::isRequestRelevant(Request request)
{
	return (request.id == SignupCode || request.id == LoginCode);
}

RequestResult LoginRequestHandler::handleRequest(Request request)
{	
	switch (request.id)
	{
	case SignupCode:
		return signup(request);

	case LoginCode:
		return login(request);

	default:
		return RequestResult();
	}
}

RequestResult LoginRequestHandler::login(Request request)
{
	LoginRequest loginReq;
	LoginResponse loginRes;

	try
	{
		loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(request.buffer);
		m_loginManager->login(loginReq.username, loginReq.password);
		loginRes.status = STATUS_SUCCEEDED;
		std::cout << loginReq.username << " logged in successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		loginRes.status = STATUS_FAILED;
	}

	std::cout << "Sending login response with status " << loginRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(loginRes);
	
	if (loginRes.status)
	{
		reqResult.newHandler = m_handlerFactory->createMenuRequestHandler(m_loginManager->getUser(loginReq.username));
	}
	else
	{	
		reqResult.newHandler = nullptr;
	}

	return reqResult;
}

RequestResult LoginRequestHandler::signup(Request request)
{
	SignupRequest signupReq;
	SignupResponse signupRes;

	try
	{
		signupReq = JsonRequestPacketDeserializer::deserializeSignupRequest(request.buffer);
		m_loginManager->signup(signupReq.username, signupReq.password, signupReq.email);
		signupRes.status = STATUS_SUCCEEDED;
		std::cout << signupReq.username << " signed up successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		signupRes.status = STATUS_FAILED;
	}

	std::cout << "Sending signup response with status " << signupRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(signupRes);

	if (signupRes.status)
	{
		reqResult.newHandler = m_handlerFactory->createMenuRequestHandler(m_loginManager->getUser(signupReq.username));
	}
	else
	{
		reqResult.newHandler = nullptr;
	}

	return reqResult;	
}

/******************************************* MenuRequestHandler *******************************************/

MenuRequestHandler::MenuRequestHandler(LoggedUser* user, 
									LoginManager* loginManager,
									RoomManager* roomManager, 
									HighscoreTable* highscoreTable, 
									RequestHandlerFactory& handlerFactory)
{
	m_user = user;
	m_loginManager = loginManager;
	m_roomManager = roomManager;
	m_highscoreTable = highscoreTable;
	m_handlerFactory = &handlerFactory;
}

MenuRequestHandler::~MenuRequestHandler()
{

}

bool MenuRequestHandler::isRequestRelevant(Request request)
{
	return (request.id == LogoutCode || 
		request.id == GetRoomsCode ||
		request.id == GetPlayersInRoomCode ||
		request.id == JoinRoomCode ||
		request.id == CreateRoomCode ||
		request.id == HighscoreCode ||
		request.id == UserStatusCode);
}

RequestResult MenuRequestHandler::handleRequest(Request request)
{
	switch (request.id)
	{
	case LogoutCode:
		return logout(request);

	case GetRoomsCode:
		return getRooms(request);

	case GetPlayersInRoomCode:
		return getPlayers(request);

	case JoinRoomCode:
		return joinRoom(request);

	case CreateRoomCode:
		return createRoom(request);

	case HighscoreCode:
		return getHighscores(request);

	case UserStatusCode:
		return getUserStatus(request);

	default:
		return RequestResult();
	}
}

RequestResult MenuRequestHandler::logout(Request request)
{
	LogoutResponse logoutRes;

	try
	{
		std::string username = m_user->getUsername();
		m_loginManager->logout(username);
		logoutRes.status = STATUS_SUCCEEDED;
		std::cout << username << " logged out successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		logoutRes.status = STATUS_FAILED;
	}

	std::cout << "Sending logout response with status " << logoutRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(logoutRes);

	if (logoutRes.status)
	{
		reqResult.newHandler = m_handlerFactory->createLoginRequestHandler();
	}
	else
	{
		reqResult.newHandler = nullptr;
	}

	return reqResult;
}

RequestResult MenuRequestHandler::getRooms(Request request)
{
	GetRoomsResponse getRoomsRes;

	try
	{
		getRoomsRes.rooms = m_roomManager->getRooms();
		getRoomsRes.status = STATUS_SUCCEEDED;
		std::cout << "rooms list received successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		getRoomsRes.status = STATUS_FAILED;
	}

	std::cout << "Sending rooms list response with status " << getRoomsRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(getRoomsRes);
	reqResult.newHandler = nullptr;

	return reqResult;
}

RequestResult MenuRequestHandler::getPlayers(Request request)
{
	GetPlayersInRoomRequest getPlayersInRoomReq;
	GetPlayersInRoomResponse getPlayersInRoomRes;

	try
	{
		getPlayersInRoomReq = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(request.buffer);
		getPlayersInRoomRes.players = m_roomManager->getRoom(getPlayersInRoomReq.roomId)->getAllUsersNames();
		getPlayersInRoomRes.status = STATUS_SUCCEEDED;
		std::cout << "room's players list received successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		getPlayersInRoomRes.status = STATUS_FAILED;
	}

	std::cout << "Sending room's players list response with status " << getPlayersInRoomRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(getPlayersInRoomRes);
	reqResult.newHandler = nullptr;

	return reqResult;
}

RequestResult MenuRequestHandler::joinRoom(Request request)
{
	JoinRoomRequest joinRoomReq;
	JoinRoomResponse joinRoomRes;

	try
	{
		joinRoomReq = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(request.buffer);
		m_roomManager->getRoom(joinRoomReq.roomId)->addUser(m_user);
		joinRoomRes.status = STATUS_SUCCEEDED;
		std::cout << "user joined room " << joinRoomReq.roomId << " successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		joinRoomRes.status = STATUS_FAILED;
	}

	std::cout << "Sending joinRoom response with status " << joinRoomRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(joinRoomRes);
	
	if (joinRoomRes.status)
	{
		reqResult.newHandler = m_handlerFactory->createRoomMemberRequestHandler(m_user, m_roomManager->getRoom(joinRoomReq.roomId));
	}
	else
	{
		reqResult.newHandler = nullptr;
	}

	return reqResult;
}

RoomData MenuRequestHandler::arrangeRoomData(CreateRoomRequest createRoomReq)
{
	if (createRoomReq.questionCount > MAX_NUM_OF_QUESTIONS)
	{
		throw(std::exception(ERROR_TOO_MANY_QUESTIONS));
	}

	RoomData roomData;

	roomData.id = m_roomManager->getNextId();
	roomData.name = createRoomReq.roomName;
	roomData.maxPlayer = createRoomReq.maxUsers;
	roomData.questionCount = createRoomReq.questionCount;
	roomData.timePerQuestion = createRoomReq.answerTimeout;
	roomData.hasGameBegun = GAME_NOT_STARTED;
	roomData.gameId = GAME_NOT_STARTED;

	return roomData;
}

RequestResult MenuRequestHandler::createRoom(Request request)
{
	CreateRoomRequest createRoomReq;
	CreateRoomResponse createRoomRes;
	RoomData roomData;

	try
	{
		createRoomReq = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(request.buffer);
		roomData = arrangeRoomData(createRoomReq);
		m_roomManager->createRoom(m_user, roomData);
		createRoomRes.status = STATUS_SUCCEEDED;
		std::cout << "created room successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		createRoomRes.status = STATUS_FAILED;
	}

	std::cout << "Sending createRoom response with status " << createRoomRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(createRoomRes);

	if (createRoomRes.status)
	{
		reqResult.newHandler = m_handlerFactory->createRoomAdminRequestHandler(m_user, m_roomManager->getRoom(roomData.id));
	}
	else
	{
		reqResult.newHandler = nullptr;
	}

	return reqResult;
}

RequestResult MenuRequestHandler::getHighscores(Request request)
{
	HighscoreResponse highscoreRes;

	try
	{
		highscoreRes.highscores = m_highscoreTable->arrangeHighscoresList(m_highscoreTable->getHighscores());
		highscoreRes.status = STATUS_SUCCEEDED;
		std::cout << "highscore table received successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		highscoreRes.status = STATUS_FAILED;
	}

	std::cout << "Sending highscore table response with status " << highscoreRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(highscoreRes);
	reqResult.newHandler = nullptr;

	return reqResult;
}

RequestResult MenuRequestHandler::getUserStatus(Request request)
{
	UserStatusResponse userStatusRes;

	try
	{
		userStatusRes.results = m_highscoreTable->getStatus(m_user->getUsername());
		userStatusRes.status = STATUS_SUCCEEDED;
		std::cout << "user status received successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		userStatusRes.status = STATUS_FAILED;
	}

	std::cout << "Sending user status response with status " << userStatusRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(userStatusRes);
	reqResult.newHandler = nullptr;

	return reqResult;
}

/******************************************* RoomAdminRequestHandler *******************************************/

RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser* user,
												Room* room,
												RoomManager* roomManager, 
												RequestHandlerFactory& handlerFactory)
{
	m_user = user;
	m_room = room;
	m_roomManager = roomManager;
	m_handlerFactory = &handlerFactory;
}

RoomAdminRequestHandler::~RoomAdminRequestHandler()
{

}

bool RoomAdminRequestHandler::isRequestRelevant(Request request)
{
	return (request.id == CloseRoomCode ||
		request.id == StartGameCode ||
		request.id == GetRoomStateCode);
}

RequestResult RoomAdminRequestHandler::handleRequest(Request request)
{
	switch (request.id)
	{
	case CloseRoomCode:
		return closeRoom(request);

	case StartGameCode:
		return startGame(request);

	case GetRoomStateCode:
		return getRoomState(request);

	default:
		return RequestResult();
	}
}

RequestResult RoomAdminRequestHandler::closeRoom(Request request)
{
	CloseRoomResponse closeRoomRes;

	try
	{
		m_roomManager->deleteRoom(m_room->getRoomData().id);
		m_room = nullptr;
		closeRoomRes.status = STATUS_SUCCEEDED;
		std::cout << "room closed successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		closeRoomRes.status = STATUS_FAILED;
	}

	std::cout << "Sending closeRoom response with status " << closeRoomRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(closeRoomRes);
	
	if (closeRoomRes.status)
	{
		reqResult.newHandler = m_handlerFactory->createMenuRequestHandler(m_user);
	}
	else
	{
		reqResult.newHandler = nullptr;
	}

	return reqResult;
}

RequestResult RoomAdminRequestHandler::startGame(Request request)
{
	StartGameResponse startGameRes;

	try
	{
		startGameRes.status = STATUS_SUCCEEDED;
		std::cout << "game started successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		startGameRes.status = STATUS_FAILED;
	}

	std::cout << "Sending startGame response with status " << startGameRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(startGameRes);

	if (startGameRes.status)
	{
		reqResult.newHandler = m_handlerFactory->createGameRequestHandler(m_user, m_room);
	}
	else
	{
		reqResult.newHandler = nullptr;
	}

	return reqResult;
}

void RoomAdminRequestHandler::arrangeRoomState(GetRoomStateResponse& getRoomStateRes)
{
	if (!m_room)
	{
		throw(std::exception(ERROR_ROOM_CLOSED));
	}
	
	getRoomStateRes.roomName = m_room->getRoomData().name;
	getRoomStateRes.hasGameBegun = m_room->getRoomData().hasGameBegun;
	getRoomStateRes.players = m_room->getAllUsersNames();
	getRoomStateRes.maxPlayers = m_room->getRoomData().maxPlayer;
	getRoomStateRes.questionCount = m_room->getRoomData().questionCount;
	getRoomStateRes.answerTimeout = m_room->getRoomData().timePerQuestion;
}

RequestResult RoomAdminRequestHandler::getRoomState(Request request)
{
	GetRoomStateResponse getRoomStateRes;

	try
	{
		arrangeRoomState(getRoomStateRes);
		getRoomStateRes.status = STATUS_SUCCEEDED;
		std::cout << "Room state received successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		getRoomStateRes = {};
		std::cout << error.what() << std::endl;
		getRoomStateRes.status = STATUS_FAILED;
	}

	std::cout << "Sending roomState response with status " << getRoomStateRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(getRoomStateRes);
	reqResult.newHandler = nullptr;

	return reqResult;
}

/******************************************* RoomMemberRequestHandler *******************************************/

RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser* user,
												Room* room,
												RoomManager* roomManager,
												RequestHandlerFactory& handlerFactory)
{
	m_user = user;
	m_room = room;
	m_roomManager = roomManager;
	m_handlerFactory = &handlerFactory;
}


RoomMemberRequestHandler::~RoomMemberRequestHandler()
{

}

bool RoomMemberRequestHandler::isRequestRelevant(Request request)
{
	return (request.id == LeaveRoomCode ||
		request.id == GetRoomStateCode);
}

RequestResult RoomMemberRequestHandler::handleRequest(Request request)
{
	switch (request.id)
	{
	case LeaveRoomCode:
		return leaveRoom(request);

	case GetRoomStateCode:
		return getRoomState(request);

	default:
		return RequestResult();
	}
}

RequestResult RoomMemberRequestHandler::leaveRoom(Request request)
{
	LeaveRoomResponse leaveRoomRes;

	try
	{
		m_room->removeUser(m_user->getUsername());
		leaveRoomRes.status = STATUS_SUCCEEDED;
		std::cout << "left room successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		leaveRoomRes.status = STATUS_FAILED;
	}

	std::cout << "Sending leaveRoom response with status " << leaveRoomRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(leaveRoomRes);

	if (leaveRoomRes.status)
	{
		reqResult.newHandler = m_handlerFactory->createMenuRequestHandler(m_user);
	}
	else
	{
		reqResult.newHandler = nullptr;
	}

	return reqResult;
}

void RoomMemberRequestHandler::arrangeRoomState(GetRoomStateResponse& getRoomStateRes)
{
	if (!m_room)
	{
		throw(std::exception(ERROR_ROOM_CLOSED));
	}

	getRoomStateRes.roomName = m_room->getRoomData().name;
	getRoomStateRes.hasGameBegun = m_room->getRoomData().hasGameBegun;
	getRoomStateRes.players = m_room->getAllUsersNames();
	getRoomStateRes.maxPlayers = m_room->getRoomData().maxPlayer;
	getRoomStateRes.questionCount = m_room->getRoomData().questionCount;
	getRoomStateRes.answerTimeout = m_room->getRoomData().timePerQuestion;
}

RequestResult RoomMemberRequestHandler::getRoomState(Request request)
{
	GetRoomStateResponse getRoomStateRes;

	try
	{
		arrangeRoomState(getRoomStateRes);
		getRoomStateRes.status = STATUS_SUCCEEDED;
		std::cout << "Room state received successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		getRoomStateRes = {};
		std::cout << error.what() << std::endl;
		getRoomStateRes.status = STATUS_FAILED;
	}

	std::cout << "Sending roomState response with status " << getRoomStateRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(getRoomStateRes);
	
	if (getRoomStateRes.hasGameBegun)
	{
		reqResult.newHandler = m_handlerFactory->createGameRequestHandler(m_user, m_room);
	}
	else if (getRoomStateRes.status)
	{
		reqResult.newHandler = nullptr;
	}
	else
	{
		reqResult.newHandler = m_handlerFactory->createMenuRequestHandler(m_user);
	}

	return reqResult;
}


/******************************************* GameRequestHandler *******************************************/

GameRequestHandler::GameRequestHandler(Game* game,
	LoggedUser* user,
	GameManager* gameManager,
	RequestHandlerFactory& handlerFactory)
{
	m_game = game;
	m_user = user;
	m_gameManager = gameManager;
	m_handlerFactory = &handlerFactory;
}


GameRequestHandler::~GameRequestHandler()
{

}

bool GameRequestHandler::isRequestRelevant(Request request)
{
	return (request.id == GetQuestionCode ||
		request.id == SubmitAnswerCode ||
		request.id == GetGameResultsCode ||
		request.id == LeaveGameCode);
}

RequestResult GameRequestHandler::handleRequest(Request request)
{
	switch (request.id)
	{
	case GetQuestionCode:
		return getQuestion(request);

	case SubmitAnswerCode:
		return submitAnswer(request);

	case GetGameResultsCode:
		return getGameResults(request);

	case LeaveGameCode:
		return leaveGame(request);

	default:
		return RequestResult();
	}
}

RequestResult GameRequestHandler::getQuestion(Request request)
{
	GetQuestionResponse getQuestionRes;

	try
	{
		Question* question = m_game->getQuestionForUser(m_user);

		getQuestionRes.question = question->getQuestion();
		getQuestionRes.answers = question->getPossibleAnswers();
		getQuestionRes.status = STATUS_SUCCEEDED;

		std::cout << "got question successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		getQuestionRes.status = STATUS_FAILED;
	}

	std::cout << "Sending getQuestion response with status " << getQuestionRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(getQuestionRes);

	reqResult.newHandler = nullptr;

	return reqResult;
}

RequestResult GameRequestHandler::submitAnswer(Request request)
{
	SubmitAnswerResponse submitAnswerRes;
	SubmitAnswerRequest submitAnswerReq;

	try
	{
		submitAnswerReq = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(request.buffer);
		m_game->submitAnswer(m_user, submitAnswerReq.answerId, submitAnswerReq.answerTime);
		unsigned int correctAnsId = m_game->getPlayers()[m_user].currentQuestion->getCorrectAnswerId();;

		submitAnswerRes.correctAnswerId = correctAnsId;
		submitAnswerRes.status = STATUS_SUCCEEDED;

		std::cout << "sumbited answer successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		submitAnswerRes.status = STATUS_FAILED;
	}

	std::cout << "Sending submitAnswer response with status " << submitAnswerRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(submitAnswerRes);

	reqResult.newHandler = nullptr;

	return reqResult;
}

RequestResult GameRequestHandler::getGameResults(Request request)
{
	GetGameResultsResponse getGameResultsRes;

	try
	{
		std::map<std::string, unsigned int> playersResults;
		
		m_game->waitForAllPlayers(m_user);

		std::map<LoggedUser*, GameData> players = m_game->getPlayers();
		std::map<LoggedUser*, GameData>::iterator it;
		for (it = players.begin(); it != players.end(); ++it)
		{
			playersResults.insert(std::pair<std::string, unsigned int>(it->first->getUsername(), it->second.correctAnswerCount));
		}

		getGameResultsRes.results = playersResults;
		getGameResultsRes.status = STATUS_SUCCEEDED;
		std::cout << "got game results successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		getGameResultsRes.status = STATUS_FAILED;
	}

	std::cout << "Sending gameResults response with status " << getGameResultsRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(getGameResultsRes);

	reqResult.newHandler = nullptr;


	return reqResult;
}

RequestResult GameRequestHandler::leaveGame(Request request)
{
	LeaveGameResponse leaveGameRes;

	try
	{
		m_game->removePlayer(m_user);
		
		if (m_game->getPlayers().empty())
		{
			m_gameManager->deleteGame(m_game);
		}

		leaveGameRes.status = STATUS_SUCCEEDED;
		std::cout << "left successfully" << std::endl;
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		leaveGameRes.status = STATUS_FAILED;
	}

	std::cout << "Sending leaveGame response with status " << leaveGameRes.status << " to the client" << std::endl;

	RequestResult reqResult;
	reqResult.response = JsonResponsePacketSerializer::serializeResponse(leaveGameRes);

	if (leaveGameRes.status)
	{
		reqResult.newHandler = m_handlerFactory->createMenuRequestHandler(m_user);
	}
	else
	{
		reqResult.newHandler = nullptr;
	}

	return reqResult;
}