#pragma once

#include "RequestHandlerFactory.h"
#include <vector>
#include <iostream>
#include "Managers.h"
#include "Helper.h"
#include "Request.h"
#include "Response.h"

#define STATUS_SUCCEEDED 1
#define STATUS_FAILED 0

class IRequestHandler;
class RequestHandlerFactory;
class LoginManager;
class RoomManager;
struct PlayerResults;
class HighscoreTable;

struct Request
{
	int id;
	std::string recievalTime;
	std::vector<Byte> buffer;
};

struct RequestResult
{
	std::vector<Byte> response;
	IRequestHandler* newHandler;
};


class IRequestHandler
{
public:	
	virtual ~IRequestHandler() = 0;

	virtual bool isRequestRelevant(Request request) = 0;
	virtual RequestResult handleRequest(Request request) = 0;
};

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager* loginManager, RequestHandlerFactory& handlerFactory);
	~LoginRequestHandler();

	bool isRequestRelevant(Request request);
	RequestResult handleRequest(Request request);

private:
	RequestHandlerFactory* m_handlerFactory;
	LoginManager* m_loginManager;

	RequestResult login(Request request);
	RequestResult signup(Request request);
};

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(LoggedUser* user, 
					LoginManager* loginManager, 
					RoomManager* roomManager, 
					HighscoreTable* highscoreTable, 
					RequestHandlerFactory& handlerFactory);
	
	~MenuRequestHandler();

	bool isRequestRelevant(Request request);
	RequestResult handleRequest(Request request);

private:
	LoggedUser* m_user;
	LoginManager* m_loginManager;
	RoomManager* m_roomManager;
	HighscoreTable* m_highscoreTable;
	RequestHandlerFactory* m_handlerFactory;

	RoomData arrangeRoomData(CreateRoomRequest createRoomReq);

	RequestResult logout(Request request);
	RequestResult getRooms(Request request);
	RequestResult getPlayers(Request request);
	RequestResult joinRoom(Request request);
	RequestResult createRoom(Request request);
	RequestResult getHighscores(Request request);
	RequestResult getUserStatus(Request request);
};

class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(LoggedUser* user,
							Room* room,
							RoomManager* roomManager,
							RequestHandlerFactory& handlerFactory);

	~RoomAdminRequestHandler();

	bool isRequestRelevant(Request request);
	RequestResult handleRequest(Request request);

private:
	LoggedUser* m_user;
	Room* m_room;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handlerFactory;

	void arrangeRoomState(GetRoomStateResponse& getRoomStateRes);

	RequestResult closeRoom(Request request);
	RequestResult startGame(Request request);
	RequestResult getRoomState(Request request);
};

class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(LoggedUser* user,
							Room* room,
							RoomManager* roomManager,
							RequestHandlerFactory& handlerFactory);

	~RoomMemberRequestHandler();

	bool isRequestRelevant(Request request);
	RequestResult handleRequest(Request request);

private:
	LoggedUser* m_user;
	Room* m_room;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handlerFactory;

	void arrangeRoomState(GetRoomStateResponse& getRoomStateRes);

	RequestResult leaveRoom(Request request);
	RequestResult getRoomState(Request request);
};

class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(Game* game,
		LoggedUser* user,
		GameManager* gameManager,
		RequestHandlerFactory& handlerFactory);

	~GameRequestHandler();

	bool isRequestRelevant(Request request);
	RequestResult handleRequest(Request request);

private:
	Game* m_game;
	LoggedUser* m_user;
	GameManager* m_gameManager;
	RequestHandlerFactory* m_handlerFactory;

	RequestResult getQuestion(Request request);
	RequestResult submitAnswer(Request request);
	RequestResult getGameResults(Request request);
	RequestResult leaveGame(Request request);

};