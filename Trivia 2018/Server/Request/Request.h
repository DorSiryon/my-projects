#pragma once

#include <iostream>
#include <vector>
#include "Helper.h"

struct LoginRequest
{
	std::string username;
	std::string password;
};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct SubmitAnswerRequest
{
	unsigned int answerId;
	unsigned int answerTime;
};

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(std::vector<Byte> buffer);
	static SignupRequest deserializeSignupRequest(std::vector<Byte> buffer);

	static GetPlayersInRoomRequest deserializeGetPlayersRequest(std::vector<Byte> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<Byte> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<Byte> buffer);

	static SubmitAnswerRequest deserializeSubmitAnswerRequest(std::vector<Byte> buffer);
};
