#include "Request.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<Byte> buffer)
{
	LoginRequest loginReq;

	json j = json::parse(buffer.begin(), buffer.end());

	loginReq.username = j.at("username").get<std::string>();
	loginReq.password = j.at("password").get<std::string>();

	return loginReq;
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<Byte> buffer)
{
	SignupRequest signupReq;

	json j = json::parse(buffer.begin(), buffer.end());

	signupReq.username = j.at("username").get<std::string>();
	signupReq.password = j.at("password").get<std::string>();
	signupReq.email = j.at("email").get<std::string>();

	return signupReq;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(std::vector<Byte> buffer)
{
	GetPlayersInRoomRequest getPlayersInRoomReq;

	json j = json::parse(buffer.begin(), buffer.end());

	getPlayersInRoomReq.roomId = j.at("roomId").get<unsigned int>();

	return getPlayersInRoomReq;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<Byte> buffer)
{
	JoinRoomRequest joinRoomRequestReq;

	json j = json::parse(buffer.begin(), buffer.end());

	joinRoomRequestReq.roomId = j.at("roomId").get<unsigned int>();

	return joinRoomRequestReq;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<Byte> buffer)
{
	CreateRoomRequest createRoomRequestReq;

	json j = json::parse(buffer.begin(), buffer.end());

	createRoomRequestReq.roomName = j.at("roomName").get<std::string>();
	createRoomRequestReq.maxUsers = j.at("maxUsers").get<unsigned int>();
	createRoomRequestReq.questionCount = j.at("questionCount").get<unsigned int>();
	createRoomRequestReq.answerTimeout = j.at("answerTimeout").get<unsigned int>();

	return createRoomRequestReq;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(std::vector<Byte> buffer)
{
	SubmitAnswerRequest submitAnswerRequestReq;

	json j = json::parse(buffer.begin(), buffer.end());

	submitAnswerRequestReq.answerId = j.at("answerId").get<unsigned int>();
	submitAnswerRequestReq.answerTime = j.at("answerTime").get<unsigned int>();

	return submitAnswerRequestReq;
}
