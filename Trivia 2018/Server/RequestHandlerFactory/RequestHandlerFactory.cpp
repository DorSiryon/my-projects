#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory(IDatabase* database)
{
	m_loginManager = new LoginManager(database);
	m_roomManager = new RoomManager();
	m_highscoreTable = new HighscoreTable(database);
	m_gameManager = new GameManager(database);
}

RequestHandlerFactory::~RequestHandlerFactory()
{
	if (m_loginManager)
	{
		delete m_loginManager;
		m_loginManager = nullptr;
	}

	if (m_roomManager)
	{
		delete m_roomManager;
		m_roomManager = nullptr;
	}

	if (m_highscoreTable)
	{
		delete m_highscoreTable;
		m_highscoreTable = nullptr;
	}

	if (m_gameManager)
	{
		delete m_gameManager;
		m_gameManager = nullptr;
	}
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	LoginRequestHandler* handler = new LoginRequestHandler(m_loginManager, *this);

	return handler;
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser* user)
{
	MenuRequestHandler* handler = new MenuRequestHandler(user, m_loginManager, m_roomManager, m_highscoreTable, *this);

	return handler;
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser* user, Room* room)
{
	RoomAdminRequestHandler* handler = new RoomAdminRequestHandler(user, room, m_roomManager, *this);

	return handler;
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser* user, Room* room)
{
	RoomMemberRequestHandler* handler = new RoomMemberRequestHandler(user, room, m_roomManager, *this);

	return handler;
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser* user, Room* room)
{
	GameRequestHandler* handler;
	unsigned int gameId = room->getRoomData().gameId;
	
	if (gameId == GAME_NOT_STARTED)
	{
		handler = new GameRequestHandler(m_gameManager->createGame(room), user, m_gameManager, *this);
	}
	else
	{
		handler = new GameRequestHandler(m_gameManager->findGame(gameId), user, m_gameManager, *this);
	}

	room->removeUser(user->getUsername());

	if (room->getAllUsers().empty())
	{
		m_roomManager->deleteRoom(room->getRoomData().id);
	}

	return handler;
}