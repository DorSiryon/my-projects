#pragma once

#include <iostream>
#include <map>
#include <list>
#include <thread>
#include <WinSock2.h>
#include <Windows.h>
#include "Database.h"
#include "Handlers.h"
#include "Managers.h"


class LoginManager;
class RoomManager;
class HighscoreTable;
class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;
class IRequestHandler;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory(IDatabase* database);
	~RequestHandlerFactory();

	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser* user);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser* user, Room* room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser* user, Room* room);
	GameRequestHandler* createGameRequestHandler(LoggedUser* user, Room* room);

private:
	LoginManager* m_loginManager;
	RoomManager* m_roomManager;
	HighscoreTable* m_highscoreTable;
	GameManager* m_gameManager;
};