#pragma warning(disable : 4996)

#include "Helper.h"

std::string getPaddedNumber(int num, int digits)
{
	std::ostringstream ostr;
	ostr << std::setw(digits) << std::setfill('0') << num;
	return ostr.str();
}

std::string getLocalTimeNow()
{
	time_t now = std::time(nullptr);
	std::stringstream oss;
	oss << std::put_time(localtime(&now), "%a %b %d %H:%M:%S %Y");
	return oss.str();
}

/******************************************* Byte *******************************************/

Byte::Byte(int num)
{
	_byte = (char)num;
}

Byte::Byte(char ch)
{
	_byte = ch;
}

void Byte::operator=(char ch)
{
	_byte = ch;
}

bool Byte::operator==(Byte& other) const
{
	return (_byte == other._byte);
}

char Byte::getByte()
{
	return _byte;
}