#pragma once

#include <string>
#include <iostream>
#include "nlohmann/json.hpp"
#include <WinSock2.h>
#include <windows.h>

using json = nlohmann::json;

#define CODE_BYTES_NUM 1
#define DATA_SIZE_BYTES_NUM 4

std::string getPaddedNumber(int num, int digits);
std::string getLocalTimeNow();

enum Codes
{
	ErrorCode = 0,
	SignupCode,
	LoginCode,

	LogoutCode,
	GetRoomsCode,
	GetPlayersInRoomCode,
	JoinRoomCode,
	CreateRoomCode,
	HighscoreCode,
	UserStatusCode,

	CloseRoomCode,
	StartGameCode,
	GetRoomStateCode,
	LeaveRoomCode,

	GetQuestionCode,
	SubmitAnswerCode,
	GetGameResultsCode,
	LeaveGameCode
};

class Byte
{
public:
	Byte(int num);
	Byte(char ch);

	void operator=(char ch);
	bool operator==(Byte& other) const;
	char getByte();

private:
	char _byte;
};

