#pragma comment(lib, "ws2_32.lib")

#include <iostream>
#include <exception>
#include "WSAInitializer.h"
#include "Server.h"

void main()
{
	try
	{
		WSAInitializer wsaInit;
		Server triviaServer = Server();

		triviaServer.run();
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
	}

	system("PAUSE");
}