#pragma once

#include <iostream>
#include <map>
#include <list>
#include <vector>
#include <thread>
#include <string>
#include <WinSock2.h>
#include <Windows.h>
#include "Database.h"
#include "Response.h"
#include "Managers.h"
#include "Handlers.h"
#include "RequestHandlerFactory.h"
#include "Helper.h"

#define PORT 498

#define ERROR_REQ_CODE_DOESNT_MATCH "ERROR - request code does not match the request that should be sent"

class Communicator
{
public:
	Communicator(IDatabase* database);
	~Communicator();

	void bindAndListen(int port);
	void handleRequests(SOCKET client_socket);

private:
	std::map<SOCKET, IRequestHandler*> m_clients;
	SOCKET _serverSocket;
	RequestHandlerFactory* m_handlerFactory;

	Request getClientRequest(SOCKET client_socket);
	void parseRequestFromSocket(SOCKET client_socket, Request& request);

	void startThreadForNewClient(SOCKET client_socket);
	void accept();

	static char* getPartFromSocket(SOCKET sc, int bytesNum);
	static int getIntPartFromSocket(SOCKET sc, int bytesNum);
	static int getAsciiPartFromSocket(SOCKET sc);
	std::vector<Byte> getCharVector(SOCKET sc, int bytesNum);
	static char* bufferToChar(std::vector<Byte> buffer);

	void sendResponse(SOCKET sc, std::vector<Byte> response);
};

class Server
{
public:
	Server();
	~Server();

	void run();

private:
	IDatabase* m_database;
	Communicator* m_communicator;
};