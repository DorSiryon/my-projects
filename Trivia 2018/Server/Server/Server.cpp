#include "Server.h"

/******************************************* Server *******************************************/

Server::Server() 
{
	m_database = new sqliteDatabase();
	m_database->open();

	m_communicator = new Communicator(m_database);
}

Server::~Server()
{
	if (m_database)
	{
		delete m_database;
		m_database = nullptr;
	}

	if (m_communicator)
	{
		delete m_communicator;
		m_communicator = nullptr;
	}
}

void Server::run()
{
	m_communicator->bindAndListen(PORT);
}

/******************************************* Communicator *******************************************/

/*
function contructs server
input: none
output: none
*/
Communicator::Communicator(IDatabase* database)
{
	m_handlerFactory = new RequestHandlerFactory(database);
	
	// notice that we step out to the global namespace
	// for the resolution of the function socket

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

}

/*
function destructs server
input: none
output: none
*/
Communicator::~Communicator()
{
	std::map<SOCKET, IRequestHandler*>::iterator it;

	for (it = m_clients.begin(); it != m_clients.end(); ++it)
	{
		if (it->second)
		{
			delete it->second;
			it->second = nullptr;
		}
	}
		
	if (m_handlerFactory)
	{
		delete m_handlerFactory;
		m_handlerFactory = nullptr;
	}

	try
	{
		::closesocket(_serverSocket);
	}
	catch (...) {}
}

/*
function prepares server to accept clients
input: port
output: none
*/
void Communicator::bindAndListen(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

										// again stepping out to the global namespace
										// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (::listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	// the main thread is only accepting clients 
	// and add then to the list of handlers
	std::cout << "Waiting for client connection request" << std::endl;
	accept();
}

/*
function accepts clients
input: none
output: none
*/
void Communicator::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket;

	while (true)
	{
		client_socket = ::accept(_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		m_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, m_handlerFactory->createLoginRequestHandler()));
		std::cout << std::endl << "Client accepted. Server and client can speak" << std::endl;
		startThreadForNewClient(client_socket);
	}

}

void Communicator::handleRequests(SOCKET cs)
{
	try
	{
		while (true)
		{
			std::cout << "Ready to receive request" << std::endl;
			Request req = getClientRequest(cs);
			if (m_clients[cs]->isRequestRelevant(req))
			{
				RequestResult res = m_clients[cs]->handleRequest(req);
				sendResponse(cs, res.response);

				if (res.newHandler)
				{
					delete m_clients[cs];
					m_clients[cs] = res.newHandler;
				}
			}
			else
			{
				ErrorResponse errorRes = { ERROR_REQ_CODE_DOESNT_MATCH };
				sendResponse(cs, JsonResponsePacketSerializer::serializeResponse(errorRes));
			}
		}
	}
	catch (std::exception& error)
	{
		std::cout << error.what() << std::endl;
		m_clients.erase(cs);
	}

}

void Communicator::startThreadForNewClient(SOCKET client_socket)
{
	std::thread t(&Communicator::handleRequests, this, client_socket);
	t.detach();
}

Request Communicator::getClientRequest(SOCKET client_socket)
{
	Request request;
	
	parseRequestFromSocket(client_socket, request);

	request.recievalTime = getLocalTimeNow();

	return request;
}

void Communicator::parseRequestFromSocket(SOCKET client_socket, Request& request)
{
	request.id = getAsciiPartFromSocket(client_socket);
	int data_size = getIntPartFromSocket(client_socket, DATA_SIZE_BYTES_NUM);
	request.buffer = getCharVector(client_socket, data_size);

	std::cout << "Received request of code " << request.id << " with size of " << data_size << std::endl;
}
	
char* Communicator::getPartFromSocket(SOCKET sc, int bytesNum)
{
	char* data = { 0 };
	
	if (bytesNum == 0)
	{
		return data;
	}

	data = new char[bytesNum + 1];
	int res = recv(sc, data, bytesNum, 0);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}

// recieves the type code of the message from socket (3 bytes)
// and returns the code. if no message found in the socket returns 0 (which means the client disconnected)
int Communicator::getIntPartFromSocket(SOCKET sc, int bytesNum)
{
	char* s = getPartFromSocket(sc, bytesNum);
	
	if (s == "")
	{
		return 0;
	}

	int res = std::atoi(s);
	
	delete[] s;
	return  res;
}

int Communicator::getAsciiPartFromSocket(SOCKET sc)
{
	char* s = getPartFromSocket(sc, 1);

	if (s == "")
	{
		return 0;
	}

	int res = (int)s[0];
	
	delete[] s;
	return  res;
}


std::vector<Byte> Communicator::getCharVector(SOCKET sc, int bytesNum)
{
	Byte ch(NULL);
	std::vector<Byte> data;

	for (int i = 0; i < bytesNum; i++)
	{
		ch = getPartFromSocket(sc, CODE_BYTES_NUM)[0];
		data.push_back(ch);
	}
	
	return data;
}

char* Communicator::bufferToChar(std::vector<Byte> buffer)
{
	char* res = new char[buffer.size()];

	for (unsigned int i = 0; i < buffer.size(); i++)
	{
		res[i] = buffer[i].getByte();
	}

	return res;
}

// send data to socket
void Communicator::sendResponse(SOCKET sc, std::vector<Byte> response)
{
	char* res = bufferToChar(response);
	
	if (send(sc, res, response.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}

	delete[] res;
}