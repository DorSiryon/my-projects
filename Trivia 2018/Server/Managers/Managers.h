#pragma once

#include <iostream>
#include <random>
#include "Database.h"
#include <vector>
#include <map>
#include <mutex>

#define INITIAL_ROOM_ID 1
#define GAME_STARTED 1
#define GAME_NOT_STARTED 0

#define ERROR_FIELD_EMPTY "ERROR - one of the fields is empty"
#define ERROR_USER_NOT_LOGGED_IN "ERROR - user is not logged in"
#define ERROR_USER_ALREADY_EXIST "ERROR - username already exists in the database"
#define ERROR_INCORRECT_PASSWORD "ERROR - incorrect password"
#define ERROR_USER_ALREADY_SIGNED_IN "ERROR - this user is already signed in"
#define ERROR_ROOM_ALREADY_FULL "ERROR - room is already full"
#define ERROR_ROOM_DOESNT_EXIST "ERROR - room does not exist"
#define ERROR_ROOM_CLOSED "ERROR - The room has been closed"
#define ERROR_USER_NOT_IN_ROOM "ERROR - user is not in the room"
#define ERROR_NO_MORE_QUESTIONS "ERROR - no more questions, game is over"
#define ERROR_TOO_MANY_QUESTIONS "ERROR - requested too many questions than possible"

class IDatabase;
struct PlayerResults;

class LoggedUser
{
public:
	LoggedUser(std::string username);

	std::string getUsername() const;

private:
	std::string m_username;
};

class LoginManager
{
public:
	LoginManager(IDatabase* database);
	~LoginManager();

	LoggedUser* getUser(std::string username);

	void signup(std::string username, std::string password, std::string email);
	void login(std::string username, std::string password);
	void logout(std::string username);


private:
	IDatabase* m_database;
	std::vector<LoggedUser*> m_loggedUsers;
};

struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayer;
	unsigned int questionCount;
	unsigned int timePerQuestion;
	unsigned int hasGameBegun;
	unsigned int gameId; //If 0 - the game hasn't begun
};

class Room
{
public:
	Room(LoggedUser* user, RoomData roomData);
	~Room();

	void addUser(LoggedUser* user);
	void removeUser(std::string username);
	void setHasGameBegun(unsigned int hasGameBegun);
	void setGameId(unsigned int gameId);

	std::vector<LoggedUser*> getAllUsers() const;
	std::vector<std::string> getAllUsersNames();
	RoomData getRoomData();

private:
	std::vector<LoggedUser*> m_users;
	RoomData m_metadata;
};

class RoomManager
{
public:
	RoomManager();
	~RoomManager();
	
	void createRoom(LoggedUser* user, RoomData roomData);
	void deleteRoom(unsigned int id);
	std::vector<RoomData> getRooms();
	Room* getRoom(unsigned int id);
	unsigned int getRoomState(unsigned int id);

	unsigned int getNextId();

private:
	std::map<unsigned int, Room*> m_rooms;
	unsigned int m_nextId;

	bool doesRoomExist(unsigned int id);
};

struct Highscore
{
	std::string username;
	int score;
};

class HighscoreTable
{
public:
	HighscoreTable(IDatabase* database);

	std::map<std::string, int> getHighscores();
	std::vector<Highscore> arrangeHighscoresList(std::map<std::string, int> highscores);
	PlayerResults getStatus(std::string username);

private:
	IDatabase* m_database;
};

class Question
{
public:
	std::string getQuestion();
	std::map<unsigned int, std::string> getPossibleAnswers();
	std::string getCorrectAnswer();
	unsigned int getCorrectAnswerId();
	unsigned int getQuestionId();

	void setQuestionId(unsigned int questionId);
	void setQuestion(std::string question);
	void addAnswer(std::string answer);
	void addCorrectAnswer(std::string correctAnswer);

private:
	std::string m_question;
	unsigned int m_questionId;
	std::map<unsigned int, std::string> m_possibleAnswers;
	unsigned int m_correctAnswerId;
	std::vector<unsigned int> m_possibleAnswersIds = { 1, 2, 3, 4 };

	unsigned int getRandomId();
};

struct GameData
{
	Question* currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averangeAnswerTime;
	unsigned int numQuestions;
	unsigned int QuestionsSent;
};

class Game
{
public:
	Game(IDatabase* database, unsigned int gameId, std::vector<Question*> questions, std::map<LoggedUser*, GameData> players);
	~Game();

	void waitForAllPlayers(LoggedUser* user);
	Question* getQuestionForUser(LoggedUser* user);
	void submitAnswer(LoggedUser* user, unsigned int answerId, unsigned int answerTime);
	void removePlayer(LoggedUser* user);
	std::map<LoggedUser*, GameData> getPlayers();
	unsigned int getGameId();

private:
	IDatabase * m_database;
	unsigned int m_gameId;
	std::vector<Question*> m_questions;
	std::map<LoggedUser*, GameData> m_players;
	std::mutex m_mtx_gamePlayers;
};


class GameManager
{
public:
	GameManager(IDatabase* database);
	~GameManager();

	Game* createGame(Room* room);
	void deleteGame(Game* game);
	Game* findGame(unsigned int gameId);

private:
	IDatabase* m_database;
	std::vector<Game*> m_games;
};