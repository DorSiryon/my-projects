#include "Managers.h"

std::mutex mtx_loggedUsers;
std::mutex mtx_rooms;
std::mutex mtx_games;

/******************************************* LoginManager *******************************************/

LoginManager::LoginManager(IDatabase* database)
{
	m_database = database;
}

LoginManager::~LoginManager()
{
	std::vector<LoggedUser*>::iterator it;
	
	std::unique_lock<std::mutex> loggedUserLocker(mtx_loggedUsers);

	for (it = m_loggedUsers.begin(); it != m_loggedUsers.end(); ++it)
	{
		if (*it)
		{
			delete *it;
			*it = nullptr;
		}
	}

	loggedUserLocker.unlock();
}

/*
Function gets the user by its username.
 
Input:
	name (string).
Output:
 	LoggedUser*.
*/
LoggedUser* LoginManager::getUser(std::string username)
{
	std::vector<LoggedUser*>::iterator it;

	std::unique_lock<std::mutex> loggedUserLocker(mtx_loggedUsers);

	for (it = m_loggedUsers.begin(); it != m_loggedUsers.end(); ++it)
	{
		if ((*it)->getUsername() == username)
		{
			return *it;
		}
	}

	loggedUserLocker.unlock();
	
	throw(std::exception(ERROR_USER_NOT_LOGGED_IN));
}

/*
Function signs up the user.

Input:
	name (string), password (string), email (string).
Output:
	None.
*/

void LoginManager::signup(std::string username, std::string password, std::string email)
{
	if (username == "" || password == "" || email == "")
	{
		throw(std::exception(ERROR_FIELD_EMPTY));
	}
	
	bool doesUserExist = m_database->doesUserExist(username);

	if (doesUserExist)
	{
		throw(std::exception(ERROR_USER_ALREADY_EXIST));
	}

	m_database->addUser(username, password, email);
	LoggedUser* user = new LoggedUser(username);

	std::unique_lock<std::mutex> loggedUserLocker(mtx_loggedUsers);

	m_loggedUsers.push_back(user);

	loggedUserLocker.unlock();
}

/*
Function logs in the user.

Input:
	name (string), password (string).
Output:
	None.
*/
void LoginManager::login(std::string username, std::string password)
{
	if (username == "" || password == "")
	{
		throw(std::exception(ERROR_FIELD_EMPTY));
	}

	bool isPasswordCorrect = m_database->isPasswordCorrect(username, password);
	
	if (!isPasswordCorrect)
	{
		throw(std::exception(ERROR_INCORRECT_PASSWORD));
	}

	std::vector<LoggedUser*>::iterator it;

	std::unique_lock<std::mutex> loggedUserLocker(mtx_loggedUsers);

	for (it = m_loggedUsers.begin(); it != m_loggedUsers.end(); ++it)
	{
		if (username == (*it)->getUsername())
		{
			throw(std::exception(ERROR_USER_ALREADY_SIGNED_IN));
		}
	}

	LoggedUser* user = new LoggedUser(username);
	m_loggedUsers.push_back(user);
	
	loggedUserLocker.unlock();
}

/*
Function logs out the user.

Input:
	name (string).
Output:
	None.
*/
void LoginManager::logout(std::string username)
{
	bool userFound = false;
	std::vector<LoggedUser*>::iterator it;

	std::unique_lock<std::mutex> loggedUserLocker(mtx_loggedUsers);

	for (it = m_loggedUsers.begin(); it != m_loggedUsers.end(); ++it)
	{
		if ((*it)->getUsername() == username)
		{
			delete *it;
			*it = nullptr;
			m_loggedUsers.erase(it);
			userFound = true;
			break;
		}
	}

	loggedUserLocker.unlock();

	if (!userFound)
	{
		throw(std::exception(ERROR_USER_NOT_LOGGED_IN));
	}
}

/******************************************* LoggedUser *******************************************/

LoggedUser::LoggedUser(std::string username)
{
	m_username = username;
}

/*
Function gets username.

Input:
	None.
Output:
	username (string).
*/
std::string LoggedUser::getUsername() const
{
	return m_username;
}

/******************************************* Room *******************************************/

Room::Room(LoggedUser* user, RoomData roomData)
{
	m_metadata = roomData;
	addUser(user);
}

Room::~Room()
{

}

/*
Function adds user to the room.

Input:
	LoggedUser*.
Output:
	None.
*/
void Room::addUser(LoggedUser* user)
{
	if (m_users.size() >= m_metadata.maxPlayer)
	{
		throw(std::exception(ERROR_ROOM_ALREADY_FULL));
	}
	
	m_users.push_back(user);
}

/*
Function removes the user from the room.

Input:
	name (string).
Output:
	None.
*/
void Room::removeUser(std::string username)
{
	std::vector<LoggedUser*>::iterator it;
	bool userFound = false;

	for (it = m_users.begin(); it != m_users.end(); ++it)
	{
		if ((*it)->getUsername() == username)
		{
			m_users.erase(it);
			userFound = true;
			break;
		}
	}

	if (!userFound)
	{
		throw(std::exception(ERROR_USER_NOT_IN_ROOM));
	}
}

/*
Function sets the value hasGameBegun.

Input:
	unsigned int (1/0).
Output:
	None.
*/
void Room::setHasGameBegun(unsigned int hasGameBegun)
{
	m_metadata.hasGameBegun = hasGameBegun;
}

/*
Function sets the game id.

Input:
	unsigned int.
Output:
	None.
*/
void Room::setGameId(unsigned int gameId)
{
	m_metadata.gameId = gameId;
}

/*
Function gets all the user in the room.

Input:
	None.
Output:
	The users list (LoggedUser* Vector).
*/
std::vector<LoggedUser*> Room::getAllUsers() const
{
	return m_users;
}

/*
Function gets all the users names in the room.

Input:
	None.
Output:
	The usernames list (string Vector).
*/
std::vector<std::string> Room::getAllUsersNames()
{
	std::vector<std::string> roomUsersNames;
	std::vector<LoggedUser*>::iterator it;

	for (it = m_users.begin(); it != m_users.end(); ++it)
	{
		roomUsersNames.push_back((*it)->getUsername());
	}

	return roomUsersNames;
}

/*
Function gets the room data of the room.

Input:
	None.
Output:
	RoomData.
*/
RoomData Room::getRoomData()
{
	return m_metadata;
}

/******************************************* RoomManager *******************************************/

RoomManager::RoomManager()
{
	m_nextId = INITIAL_ROOM_ID;
}

RoomManager::~RoomManager()
{
	std::map<unsigned int, Room*>::iterator it;
	
	std::unique_lock<std::mutex> roomsLocker(mtx_rooms);

	for (it = m_rooms.begin(); it != m_rooms.end(); ++it)
	{
		if (it->second)
		{
			delete it->second;
			it->second = nullptr;
		}
	}

	roomsLocker.unlock();

}

/*
Function creates room.

Input:
	user (LoggedUser*), RoomData.
Output:
	None.
*/
void RoomManager::createRoom(LoggedUser* user, RoomData roomData)
{
	Room* room = new Room(user, roomData);

	std::unique_lock<std::mutex> roomsLocker(mtx_rooms);

	m_rooms.insert(std::pair<unsigned int, Room*>(roomData.id, room));

	roomsLocker.unlock();
}

/*
Function deletes a room.

Input:
	id (unsigned int).
Output:
	None.
*/
void RoomManager::deleteRoom(unsigned int id)
{
	if (doesRoomExist(id))
	{
		throw(std::exception(ERROR_ROOM_DOESNT_EXIST));
	}
	
	std::unique_lock<std::mutex> roomsLocker(mtx_rooms);

	if (m_rooms[id])
	{
		delete m_rooms[id];
		m_rooms[id] = nullptr;
	}

	m_rooms.erase(id);

	roomsLocker.unlock();
}

/*
Function gets all rooms data.

Input:
	None.
Output:
	rooms (RoomData Vector)
*/
std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> rooms;
	std::map<unsigned int, Room*>::iterator it;

	std::unique_lock<std::mutex> roomsLocker(mtx_rooms);

	for (it = m_rooms.begin(); it != m_rooms.end(); ++it)
	{
		rooms.push_back(it->second->getRoomData());
	}

	roomsLocker.unlock();

	return rooms;
}

/*
Function gets a room.

Input:
	Room id (unsigned int).
Output:W
	Room*.
*/
Room* RoomManager::getRoom(unsigned int id)
{
	if (doesRoomExist(id))
	{
		throw(std::exception(ERROR_ROOM_DOESNT_EXIST));
	}
	
	std::unique_lock<std::mutex> roomsLocker(mtx_rooms);

	Room* room = m_rooms[id];

	roomsLocker.unlock();

	return room;
}

/*
Function gets whether the game has begun or not.

Input:
	room id (unsigned int).
Output:
	unsigned int (0/1).
*/
unsigned int RoomManager::getRoomState(unsigned int id)
{
	if (doesRoomExist(id))
	{
		throw(std::exception(ERROR_ROOM_DOESNT_EXIST));
	}

	std::unique_lock<std::mutex> roomsLocker(mtx_rooms);

	unsigned int hasGameBegun = m_rooms[id]->getRoomData().hasGameBegun;

	roomsLocker.unlock();

	return hasGameBegun;
}

/*
Function gets the next id.

Input:
	None.
Output:
	next room id (unsigned int).
*/
unsigned int RoomManager::getNextId()
{
	unsigned int nextId = m_nextId;
	m_nextId++;
	return nextId;
}

/*
Function gets whether the room exist.

Input:
	room id (unsigned int).
Output:
	bool (true/false).
*/
bool RoomManager::doesRoomExist(unsigned int id)
{
	std::unique_lock<std::mutex> roomsLocker(mtx_rooms);

	bool doesExist = (m_nextId <= id || !m_rooms[id]);

	roomsLocker.unlock();

	return doesExist;
}


/******************************************* HighscoreTable *******************************************/

HighscoreTable::HighscoreTable(IDatabase* database)
{
	m_database = database;
}

std::map<std::string, int> HighscoreTable::getHighscores()
{
	return m_database->getHighscores();
}

std::vector<Highscore> HighscoreTable::arrangeHighscoresList(std::map<std::string, int> highscores)
{
	std::vector<Highscore> highscoreList;
	std::map<std::string, int>::iterator it;

	for (it = highscores.begin(); it != highscores.end(); ++it)
	{
		highscoreList.push_back(Highscore{ it->first, it->second });
	}

	return highscoreList;
}

PlayerResults HighscoreTable::getStatus(std::string username)
{
	return m_database->getUserStatisticss(username);
}

/******************************************* Question *******************************************/

std::string Question::getQuestion()
{
	return m_question;
}

std::map<unsigned int, std::string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

std::string Question::getCorrectAnswer()
{
	return m_possibleAnswers[m_correctAnswerId];
}

unsigned int Question::getCorrectAnswerId()
{
	return m_correctAnswerId;
}

unsigned int Question::getQuestionId()
{
	return m_questionId;
}

void Question::setQuestionId(unsigned int questionId)
{
	m_questionId = questionId;
}

void Question::setQuestion(std::string question)
{
	m_question = question;
}

void Question::addAnswer(std::string answer)
{
	bool inserted = false;

	while (!inserted)
	{
		unsigned int id = getRandomId();

		if (m_possibleAnswers.find(id) == m_possibleAnswers.end())
		{
			m_possibleAnswers.insert(std::pair<unsigned int, std::string>(id, answer));
			inserted = true;
		}
	}
}

void Question::addCorrectAnswer(std::string correctAnswer)
{
	bool inserted = false;

	while (!inserted)
	{
		unsigned int id = getRandomId();

		if (m_possibleAnswers.find(id) == m_possibleAnswers.end())
		{
			m_possibleAnswers.insert(std::pair<unsigned int, std::string>(id, correctAnswer));
			m_correctAnswerId = id;
			inserted = true;
		}
	}
}

unsigned int Question::getRandomId()
{
	std::random_device r;
	std::default_random_engine e1(r());
	std::uniform_int_distribution<unsigned int> uniform_dist(1, 4);
	unsigned int id = uniform_dist(e1);

	return id;
}


/******************************************* Game *******************************************/

Game::Game(IDatabase* database, unsigned int gameId, std::vector<Question*> questions, std::map<LoggedUser*, GameData> players)
{
	m_database = database;
	m_gameId = gameId;
	m_questions = questions;
	m_players = players;
}

Game::~Game()
{
	std::vector<Question*>::iterator questionIt;
	std::map<LoggedUser*, GameData>::iterator playersIt;


	for (questionIt = m_questions.begin(); questionIt != m_questions.end(); ++questionIt)
	{
		if (*questionIt)
		{
			delete *questionIt;
			*questionIt = nullptr;
		}
	}

	for (playersIt = m_players.begin(); playersIt != m_players.end(); ++playersIt)
	{
		removePlayer(playersIt->first);
	}

}

void Game::waitForAllPlayers(LoggedUser* user)
{
	while (true)
	{
		bool toBreak = true;

		std::map<LoggedUser*, GameData>::iterator it;
		
		std::unique_lock<std::mutex> gamePlayersLocker(m_mtx_gamePlayers);

		unsigned int qNum = m_players[user].numQuestions;
		unsigned int qSent = m_players[user].QuestionsSent;

		for (it = m_players.begin(); it != m_players.end(); ++it)
		{
			//if the other user answered questions number doesn't equal this user answered questions number
			if (it->second.numQuestions != qNum)
			{
				//if the questions sent for this user is bigger or if the answered questions number of this user is bigger
				if (it->second.QuestionsSent < qSent || it->second.numQuestions < qNum)
				{
					toBreak = false;
					break;
				}
			}
		}

		gamePlayersLocker.unlock();

		if (toBreak)
		{
			break;
		}
	}
}

Question* Game::getQuestionForUser(LoggedUser* user)
{
	std::unique_lock<std::mutex> gamePlayersLocker(m_mtx_gamePlayers);

	GameData& playerGameData = m_players[user];

	gamePlayersLocker.unlock();

	if (playerGameData.numQuestions >= m_questions.size())
	{
		throw(std::exception(ERROR_NO_MORE_QUESTIONS));
	}

	waitForAllPlayers(user);
	
	playerGameData.currentQuestion = m_questions[playerGameData.numQuestions];

	playerGameData.QuestionsSent++;

	Question* currQuestion = playerGameData.currentQuestion;

	return currQuestion;
}

void Game::submitAnswer(LoggedUser* user, unsigned int answerId, unsigned int answerTime)
{
	unsigned int isCorrect = 0;

	std::unique_lock<std::mutex> gamePlayersLocker(m_mtx_gamePlayers);

	Question* currQuestion = m_players[user].currentQuestion;

	if (answerId == currQuestion->getCorrectAnswerId())
	{
		m_players[user].correctAnswerCount++;
		isCorrect = 1;
	}
	else
	{
		m_players[user].wrongAnswerCount++;

	}

	m_database->addAnswer(m_gameId,
		user->getUsername(),
		currQuestion->getQuestionId(),
		currQuestion->getPossibleAnswers()[answerId],
		isCorrect,
		answerTime);

	m_players[user].numQuestions++;

	gamePlayersLocker.unlock();
}

void Game::removePlayer(LoggedUser* user)
{
	std::unique_lock<std::mutex> gamePlayersLocker(m_mtx_gamePlayers);

	m_players.erase(user);

	gamePlayersLocker.unlock();
}

std::map<LoggedUser*, GameData> Game::getPlayers()
{
	return m_players;
}

unsigned int Game::getGameId()
{
	return m_gameId;
}

/******************************************* Game Manager *******************************************/

GameManager::GameManager(IDatabase* database)
{
	m_database = database;
}

GameManager::~GameManager()
{
	std::vector<Game*>::iterator it;

	std::unique_lock<std::mutex> gamesLocker(mtx_games);

	for (it = m_games.begin(); it != m_games.end(); ++it)
	{
		deleteGame(*it);
	}

	gamesLocker.unlock();
}

Game* GameManager::createGame(Room* room)
{
	unsigned int gameId = m_database->addGame();
	room->setHasGameBegun(GAME_STARTED);
	room->setGameId(gameId);

	std::map<LoggedUser*, GameData> players;
	std::vector<LoggedUser*> users = room->getAllUsers();
	std::vector<LoggedUser*>::iterator it;

	for (it = users.begin(); it != users.end(); ++it)
	{
		GameData gameData;
		gameData.averangeAnswerTime = 0;
		gameData.correctAnswerCount = 0;
		gameData.wrongAnswerCount = 0;
		gameData.numQuestions = 0;
		gameData.QuestionsSent = 0;
		gameData.currentQuestion = nullptr;

		players.insert(std::pair<LoggedUser*, GameData>(*it, gameData));
	}

	Game* game = new Game(m_database, gameId, m_database->getQuestions(room->getRoomData().questionCount), players);

	std::unique_lock<std::mutex> gamesLocker(mtx_games);
	
	m_games.push_back(game);

	gamesLocker.unlock();

	return game;

}

void GameManager::deleteGame(Game* game)
{
	m_database->updateFinishedGame(game->getGameId());
	delete game;
}

Game* GameManager::findGame(unsigned int gameId)
{
	std::vector<Game*>::iterator it;

	std::unique_lock<std::mutex> gamesLocker(mtx_games);

	for (it = m_games.begin(); it != m_games.end(); ++it)
	{
		if ((*it)->getGameId() == gameId)
		{
			gamesLocker.unlock();
			return *it;
		}
	}

	gamesLocker.unlock();
	return nullptr;
}