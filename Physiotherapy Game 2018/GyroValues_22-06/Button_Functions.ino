bool didButtonDebounce(struct debouncingButton& d_button)
{
    int reading = digitalRead(d_button._buttonPin);
    
    if (reading != d_button._lastButtonState) 
    {
      d_button._lastDebounceTime = millis();
    }

    d_button._lastButtonState = reading;
    
    if ((millis() - d_button._lastDebounceTime) > d_button._debounceDelay) 
    {
      if (reading != d_button._buttonState) 
      {
        d_button._buttonState = reading;
        
        if (d_button._buttonState == HIGH) 
        {
            return true;
        }
      }
    }

    return false;
}
