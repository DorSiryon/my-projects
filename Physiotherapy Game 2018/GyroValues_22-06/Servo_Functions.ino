void HeadServo::rotateServoToPoint(int newPosToGo)
{
    int newPos = _pos;
    
    if (_pos > newPosToGo)
    {
      newPos = _pos - _change;
    }
    else if (_pos < newPosToGo)
    {
      newPos = _pos + _change;
    }
  
    if (newPos != _pos)
    {
        _pos = newPos;
        _servo.write(_pos);
    }
}

bool goTo(int posRotate, int posX)
{
    if (servoHor._pos == posRotate && servoVer._pos == posX)
    {
      return true;
    }
    
    servoHor.rotateServoToPoint(posRotate);
    servoVer.rotateServoToPoint(posX);

    return false;
}
