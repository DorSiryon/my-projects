void firstTimeInitValues()
{
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetEuler(euler, &q);
  
        initEulerRotate = euler[0] * 180/M_PI;
        initEulerX = euler[1] * 180/M_PI;
        initEulerY = euler[2] * 180/M_PI;
        
        Serial.print("init euler\t");
        Serial.print(initEulerRotate);
        Serial.print("\t");
        Serial.print(initEulerX);
        Serial.print("\t");
        Serial.println(initEulerY);
}

bool waitForStabilization()
{
  static int timeCounter = 0;
  static bool isStable = false;
  static float lastEulerRotate = 0;

  if (!isStable)
  {
      timeCounter++;
      
      if (timeCounter > 100)
      {
          timeCounter = 0;
          
          float eulerRotate = euler[0] * 180/M_PI;
          Serial.println(eulerRotate);
          
          if ((int)eulerRotate == (int)lastEulerRotate)
          {
            isStable = true;
            Serial.println("Ready!");
            digitalWrite(yellow_ledPin, LOW);
          }
        
          lastEulerRotate = eulerRotate;
      }
  }
  
  return isStable;
}

void initValues()
{
    initEulerRotate = euler[0] * 180/M_PI;
    initEulerX = euler[1] * 180/M_PI;
    initEulerY = euler[2] * 180/M_PI;
    
    Serial.print("init euler\t");
    Serial.print(initEulerRotate);
    Serial.print("\t");
    Serial.print(initEulerX);
    Serial.print("\t");
    Serial.println(initEulerY);
}

bool measureValues(int Rotate, int X)
{          
    eulerRotate = euler[0] * 180/M_PI;
    eulerX = euler[1] * 180/M_PI;
    eulerY = euler[2] * 180/M_PI;

    eulerRotate = eulerRotate - initEulerRotate;
    eulerX = eulerX - initEulerX;
    eulerY = eulerY - initEulerY;
    
    Serial.print("measure euler\t");
    Serial.print(eulerRotate);
    Serial.print("\t");
    Serial.println(eulerX);

    Serial.print("Rotate: ");
    Serial.print(Rotate);
    Serial.print("\tX: ");
    Serial.println(X);

    return checkValuesAlmostEquel(eulerRotate, eulerX, Rotate, X, errorRange);
}

bool checkValuesAlmostEquel(float GyroRotate, float GyroX, float Rotate, float X, float range)
{
    return ((Rotate - range <= GyroRotate && GyroRotate <= Rotate + range) && (X - range <= GyroX && GyroX <= X + range));
}
