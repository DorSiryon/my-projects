// GyroValues 22/06/2018 
// Danny Khlebnikov, Ofek Zana, Dor Siryon 
//=========================================
//
// Changelog:
// --- 05/06/2018 -> fix levels 3-5
//
// --- 03/06/2018 -> added feature - level2, level3, level4, level5 
//
// --- 01/06/2018 -> added feature - initServo() [to fix the servo operation]
//
// --- 25/05/2018 -> added feature - level1
//
// --- 18/05/2018 -> added feature - isButtonPressed() function for the button struct
//                -> added feature - another servo
//                -> added feature - headServo struct
//                -> added feature - some useful servo functions: 
//                   rotateServoToMax(), rotateServoToMin(), rotateServoToOrigin(), rotateServoToPoint(newPos)
//                -> added feature - goTo(posRotate, posX) and goToOrigin() functions
//                -> added file - Levels_Functions
//
// --- 17/05/2018 -> made servo movement start only after gyro stabilization
//                -> divided into files: Button_Functions, Gyro_ValuesFunctions, Servo_Functions
//                -> added feature - first time initialization right after stabilized
//                -> made the software support the new 5 levels buttons system
//                -> added feature - Button and debouncingButton structs
//                -> added feature - didButtonDebounce() function
//
// --- 11/05/2018 -> added feature - servo rotation
//
// --- 04/05/2018 -> fix functions measureValues(), initValues()
//                -> added feature - wait for stabilization
//      
// --- 27/04/2018 -> added buttons for measure and initialization
//                -> added feature - initialization values
//                -> added feature - measure values from zero point
//                -> added feature - measure change values
//
// --- 20/04/2018 -> first launch 
//
//=================================================================//

#include <Servo.h>
#include "I2Cdev.h"
#include "MPU6050_6Axis_MotionApps20.h"

// Arduino Wire library is required if I2Cdev I2CDEV_ARDUINO_WIRE implementation
// is used in I2Cdev.h
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

// class default I2C address is 0x68

MPU6050 mpu;

/* =========================================================================
   NOTE: In addition to connection 3.3v, GND, SDA, and SCL, this sketch
   depends on the MPU-6050's INT pin being connected to the Arduino's
   external interrupt #0 pin. On the Arduino Uno and Mega 2560, this is
   digital I/O pin 2.
 * ========================================================================= */

// uncomment "OUTPUT_READABLE_EULER" if you want to see Euler angles
// (in degrees) calculated from the quaternions coming from the FIFO.
// Note that Euler angles suffer from gimbal lock (for more info, see
// http://en.wikipedia.org/wiki/Gimbal_lock)
#define OUTPUT_READABLE_EULER

// pins definitions:
//-------------------
#define LED_PIN 13

const int init_buttonPin = 3;
const int start_buttonPin = 4;

const int headHor_servoPin = 9;
const int headVer_servoPin = 11;

const int yellow_ledPin = 5;
const int green_ledPin = 6;
const int red_ledPin = 7;
//-------------------------------

struct Button
{
  //LOW = 0 = not pressed
  //HIGH = 1 = pressed
  Button::Button(int buttonPin)
  {
      _buttonPin = buttonPin;
      pinMode(buttonPin, INPUT);
  }

  bool Button::isButtonPressed()
  {
    return (digitalRead(_buttonPin) == HIGH);
  }
  
  int _buttonPin;
};

struct debouncingButton : public Button
{
  debouncingButton::debouncingButton(int buttonPin) : Button(buttonPin)
  {
      _buttonState = 1;
      _lastButtonState = LOW;
      _lastDebounceTime = 0;
      _debounceDelay = 50;
  }
  
  int _buttonState;
  int _lastButtonState;
  unsigned long _lastDebounceTime;
  unsigned long _debounceDelay;
};
//---------------------------------

#define SERVO_ORIGIN 90
#define SERVO_CHANGE 1

#define HOR_RANGE 180
#define VER_RANGE 120

struct HeadServo
{
  HeadServo::HeadServo(int pinNum, int motionRange) : _servo()
  {
      _pinNum = pinNum;
      
      _pos = SERVO_ORIGIN;
      _change = SERVO_CHANGE;
      _direction = 1;
      _timeCounter = 3;

      _minValue = SERVO_ORIGIN - (motionRange / 2);
      _maxValue = SERVO_ORIGIN + (motionRange / 2);
  }

  HeadServo::initServo()
  {
      _servo.attach(_pinNum);
      _servo.write(_pos);    
  }
  
  void rotateServoToPoint(int newPosToGo);

  Servo _servo;
  int _pinNum;
  
  int _pos;
  int _change;
  int _direction; // can be only 1 or -1
  int _timeCounter;

  int _minValue;
  int _maxValue;
};

struct HeadServo servoHor(headHor_servoPin, HOR_RANGE);
struct HeadServo servoVer(headVer_servoPin, VER_RANGE);

bool blinkState = false;

// MPU control/status vars
bool dmpReady = false;  // set true if DMP init was successful
uint8_t mpuIntStatus;   // holds actual interrupt status byte from MPU
uint8_t devStatus;      // return status after each device operation (0 = success, !0 = error)
uint16_t packetSize;    // expected DMP packet size (default is 42 bytes)
uint16_t fifoCount;     // count of all bytes currently in FIFO
uint8_t fifoBuffer[64]; // FIFO storage buffer

// orientation/motion vars
Quaternion q;           // [w, x, y, z]         quaternion container
VectorInt16 aa;         // [x, y, z]            accel sensor measurements
VectorInt16 aaReal;     // [x, y, z]            gravity-free accel sensor measurements
VectorInt16 aaWorld;    // [x, y, z]            world-frame accel sensor measurements
VectorFloat gravity;    // [x, y, z]            gravity vector
float euler[3];         // [psi, theta, phi]    Euler angle container
float ypr[3];           // [yaw, pitch, roll]   yaw/pitch/roll container and gravity vector

// packet structure for InvenSense teapot demo
uint8_t teapotPacket[14] = { '$', 0x02, 0,0, 0,0, 0,0, 0,0, 0x00, 0x00, '\r', '\n' };

//Gyro's origin (נק' האפס של הג'יירו)
float initEulerRotate = 0;
float initEulerX = 0;
float initEulerY = 0;

//Gyro's last values
float lastEulerRotate = 0;
float lastEulerX = 0;
float lastEulerY = 0;

//Gyro's values
float eulerRotate = 0;
float eulerX = 0;
float eulerY = 0;

//if we need to initialize the time counter
bool initCounter = false;

bool isFirstTime = true;

struct debouncingButton startButton(start_buttonPin);
struct debouncingButton initButton(init_buttonPin);

bool startLevel = false;
int levelNum = 1;
bool recvValues = false;
int errorRange = 10;

unsigned long levelStartedTime = 0;
int timeUntilLevelFailed = 20000;

// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
    mpuInterrupt = true;
}



// ================================================================
// ===                      INITIAL SETUP                       ===
// ================================================================

void setup() {
    // join I2C bus (I2Cdev library doesn't do this automatically)
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
        TWBR = 24; // 400kHz I2C clock (200kHz if CPU is 8MHz)
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif

    // initialize serial communication
    Serial.begin(115200);
    while (!Serial);

    // configure pins' mode
    pinMode(LED_PIN, OUTPUT);
    pinMode(yellow_ledPin,OUTPUT);
    pinMode(green_ledPin,OUTPUT);
    pinMode(red_ledPin,OUTPUT);
    servoHor.initServo();
    servoVer.initServo();
    
    // initialize Gyro
    Serial.println(F("Initializing I2C devices..."));
    mpu.initialize();

    // verify connection
    Serial.println(F("Testing device connections..."));
    Serial.println(mpu.testConnection() ? F("MPU6050 connection successful") : F("MPU6050 connection failed"));

    // wait for ready
    //Serial.println(F("\nSend any character to begin DMP programming and demo: "));
    //while (Serial.available() && Serial.read()); // empty buffer
    //while (!Serial.available());                 // wait for data
    //while (Serial.available() && Serial.read()); // empty buffer again

    // load and configure the DMP
    Serial.println(F("Initializing DMP..."));
    devStatus = mpu.dmpInitialize();

    // setting Gyro offsets
    mpu.setXGyroOffset(220);
    mpu.setYGyroOffset(76);
    mpu.setZGyroOffset(-85);
    mpu.setZAccelOffset(1788); // 1688 factory default for my test chip

    // make sure DMP initialization worked (returns 0 if so)
    if (devStatus == 0) {
        // turns on the DMP, now that it's ready
        Serial.println(F("Enabling DMP..."));
        mpu.setDMPEnabled(true);

        // enable Arduino interrupt detection
        Serial.println(F("Enabling interrupt detection (Arduino external interrupt 0)..."));
        attachInterrupt(0, dmpDataReady, RISING);
        mpuIntStatus = mpu.getIntStatus();

        // set our DMP Ready flag so the main loop() function knows it's okay to use it
        Serial.println(F("DMP ready! Waiting for first interrupt..."));
        dmpReady = true;
        Serial.println("Waiting for the gyro to stabilize...");
        digitalWrite(yellow_ledPin, HIGH);
        
        // get expected DMP packet size for later comparison
        packetSize = mpu.dmpGetFIFOPacketSize();
    } else {
        // ERROR!
        // 1 = initial memory load failed
        // 2 = DMP configuration updates failed
        // (if it's going to break, usually the code will be 1)
        Serial.print(F("DMP Initialization failed (code "));
        Serial.print(devStatus);
        Serial.println(F(")"));
        digitalWrite(red_ledPin, HIGH);
    }
}


// ================================================================
// ===                    MAIN PROGRAM LOOP                     ===
// ================================================================

void loop() {
    // if programming failed, don't try to do anything
    if (!dmpReady) return;

    // wait for MPU interrupt or extra packet(s) available
    while (!mpuInterrupt && fifoCount < packetSize) {
    }

    // reset interrupt flag and get INT_STATUS byte
    mpuInterrupt = false;
    mpuIntStatus = mpu.getIntStatus();

    // get current FIFO count
    fifoCount = mpu.getFIFOCount();

    // check for overflow (this should never happen unless our code is too inefficient)
    if ((mpuIntStatus & 0x10) || fifoCount == 1024) {
        // reset so we can continue cleanly
        mpu.resetFIFO();
        Serial.println(F("FIFO overflow!"));

    // otherwise, check for DMP data ready interrupt (this should happen frequently)
    } else if (mpuIntStatus & 0x02) {
        // wait for correct available data length, should be a VERY short wait
        while (fifoCount < packetSize) fifoCount = mpu.getFIFOCount();

        // read a packet from FIFO
        mpu.getFIFOBytes(fifoBuffer, packetSize);
        
        // track FIFO count here in case there is > 1 packet available
        // (this lets us immediately read more without waiting for an interrupt)
        fifoCount -= packetSize;
        
        // display Euler angles in degrees
        mpu.dmpGetQuaternion(&q, fifoBuffer);
        mpu.dmpGetEuler(euler, &q);
        
        // wait until the gyro is stabilized
        if (waitForStabilization())
        {
            // initializing the gyro's origin
            if (isFirstTime)
            {
                isFirstTime = false;
                firstTimeInitValues();
            }
    
            // initialize gyro's origin if the init button was pressed
            if (didButtonDebounce(initButton))
            {
                Serial.println("init button was pressed");
                initValues();
            }

            if (!recvValues)
            {
                // start a level if the start button was pressed
                if (didButtonDebounce(startButton))
                {
                    Serial.println("start button was pressed");
                    startLevel = true;
                    digitalWrite(red_ledPin, LOW);
                    digitalWrite(green_ledPin, LOW);
                    digitalWrite(yellow_ledPin, HIGH);
                }

                // start a level
                if (startLevel)
                {
                  switch(levelNum)
                  {
                    //level1
                    case 1:
                    {
                      if (level1())
                      {
                          startLevel = false;
                          recvValues = true;
                          digitalWrite(yellow_ledPin, LOW);
                          firstTimeInitValues();
                          levelStartedTime = millis();
                      }
                      break;
                    }
              
                    //level2
                    case 2:
                    {
                      if (level2())
                      {
                          startLevel = false;
                          recvValues = true;
                          digitalWrite(yellow_ledPin, LOW);
                          firstTimeInitValues();
                          levelStartedTime = millis();                      
                      }
                      break;
                    }
              
                    //level3
                    case 3:
                    {
                      if (level3())
                      {
                          startLevel = false;
                          recvValues = true;
                          digitalWrite(yellow_ledPin, LOW);
                          firstTimeInitValues();
                          levelStartedTime = millis();                      
                      }
                      break;
                    }
              
                    //level4
                    case 4:
                    {
                      if (level4())
                      {
                          startLevel = false;
                          recvValues = true;
                          digitalWrite(yellow_ledPin, LOW);
                          firstTimeInitValues();
                          levelStartedTime = millis();                      
                      }
                      break;
                    }
              
                    //level5
                    case 5:
                    {
                      if (level5())
                      {
                          startLevel = false;
                          recvValues = true;
                          digitalWrite(yellow_ledPin, LOW);
                          firstTimeInitValues();
                          levelStartedTime = millis();                      
                      }
                      break;
                    }
                  }
                }
            }

            // start measure gyro values
            if (recvValues)
            {
              // when 20 seconds passed - the user failed 
              if ((millis() - levelStartedTime) < timeUntilLevelFailed)
              {
                switch(levelNum)
                {
                  //level1
                  case 1:
                  {
                      if (recvLevel1())
                      {
                        recvValues = false;
                        digitalWrite(green_ledPin, HIGH);
                        levelNum++;
                      }
                      break;
                  }
  
                  //level2
                  case 2:
                  {
                      if (recvLevel2())
                      {
                        recvValues = false;
                        digitalWrite(green_ledPin, HIGH);
                        levelNum++;
                      }
                      break;
                  }
                  
                  //level3
                  case 3:
                  {
                      if (recvLevel3())
                      {
                        recvValues = false;
                        digitalWrite(green_ledPin, HIGH);
                        levelNum++;
                      }
                      break;                      
                  }
                  
                  //level4
                  case 4:
                  {
                      if (recvLevel4())
                      {
                        recvValues = false;
                        digitalWrite(green_ledPin, HIGH);
                        levelNum++;
                      }
                      break;
                  }
                  
                  //level5
                  case 5:
                  {
                      if (recvLevel5())
                      {
                        recvValues = false;
                        digitalWrite(green_ledPin, HIGH);
                        levelNum = 1;
                      }
                      break;
                  }                
                }
              }
              else
              {
                  // level failure
                  digitalWrite(red_ledPin, HIGH);
                  recvValues = false;
                  Serial.println("Level failed. Try again!");
              }
            }
        
        // blink LED to indicate activity
        blinkState = !blinkState;
        digitalWrite(LED_PIN, blinkState);
      }
  }
}

