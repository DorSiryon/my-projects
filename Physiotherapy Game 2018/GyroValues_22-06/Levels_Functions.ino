bool level1()
{
    static int moveNum = 1;
    int posRotate = SERVO_ORIGIN, posX = SERVO_ORIGIN;

    switch (moveNum)
    {
      //move #1
      case 1: 
          posRotate = SERVO_ORIGIN;
          posX = 60;
          break;
          
      //move #2
      case 2: 
          posRotate = SERVO_ORIGIN;
          posX = 120;
          break;
          
      //move #3
      case 3: 
          posRotate = SERVO_ORIGIN;
          posX = SERVO_ORIGIN;
          break;

      //return to first move    
      case 4: 
          moveNum = 1;
          return true;
    }
    
    if (goTo(posRotate, posX))
    {
        moveNum++;
    }

    return false;
}

bool level2()
{
    static int moveNum = 1;
    int posRotate = SERVO_ORIGIN, posX = SERVO_ORIGIN;

    
    switch (moveNum)
    {
      //move #1
      case 1: 
          posRotate = 150;
          posX = SERVO_ORIGIN;
          break;
          
      //move #2
      case 2: 
          posRotate = 30;
          posX = SERVO_ORIGIN;
          break;
          
      //move #3
      case 3: 
          posRotate = SERVO_ORIGIN;
          posX = SERVO_ORIGIN;
          break;

      //return to first move    
      case 4: 
          moveNum = 1;
          return true;
    }

    if (goTo(posRotate, posX))
    {
        moveNum++;
    }

    return false;
}

bool level3()
{
    static int moveNum = 1;
    int posRotate = SERVO_ORIGIN, posX = SERVO_ORIGIN;
    
    switch (moveNum)
    {
      //move #1
      case 1: 
          posRotate = SERVO_ORIGIN;
          posX = 60;
          break;
          
      //move #2
      case 2: 
          posRotate = 135;
          posX = 60;
          break;

      //move #3
      case 3: 
          posRotate = SERVO_ORIGIN;
          posX = 60;
          break;
      
      //move #4
      case 4: 
          posRotate = SERVO_ORIGIN;
          posX = SERVO_ORIGIN;
          break;

      //return to first move    
      case 5: 
          moveNum = 1;
          return true;
    }

    if (goTo(posRotate, posX))
    {
        moveNum++;
    }

    return false;
}

bool level4()
{
    static int moveNum = 1;
    int posRotate = SERVO_ORIGIN, posX = SERVO_ORIGIN;
        
    switch (moveNum)
    {
      //move #1
      case 1: 
          posRotate = SERVO_ORIGIN;
          posX = 120;
          break;
          
      //move #2
      case 2: 
          posRotate = 45;
          posX = 120;
          break;

      //move #3
      case 3: 
          posRotate = SERVO_ORIGIN;
          posX = 120;
          break;
      
      //move #4
      case 4: 
          posRotate = SERVO_ORIGIN;
          posX = SERVO_ORIGIN;
          break;
      
      //move #5
      case 5: 
          posRotate = 135;
          posX = 90;
          break;

      //move #6
      case 6: 
          posRotate = SERVO_ORIGIN;
          posX = SERVO_ORIGIN;
          break;
      
      //return to first move
      case 7: 
          moveNum = 1;
          return true;
    }

    if (goTo(posRotate, posX))
    {
        moveNum++;
    }

    return false;
}

bool level5()
{
    static int moveNum = 1;
    int posRotate = SERVO_ORIGIN, posX = SERVO_ORIGIN;
    
    switch (moveNum)
    {
      //move #1
      case 1: 
          posRotate = 45;
          posX = 90;
          break;
          
      //move #2
      case 2: 
          posRotate = 45;
          posX = 120;
          break;

      //move #3
      case 3: 
          posRotate = 135;
          posX = 120;
          break;
      
      //move #4
      case 4: 
          posRotate = 135;
          posX = 60;
          break;
      
      //move #5
      case 5: 
          posRotate = 90;
          posX = 60;
          break;

      //move #6
      case 6: 
          posRotate = SERVO_ORIGIN;
          posX = SERVO_ORIGIN;
          break;
      
      //return to first move
      case 7: 
          moveNum = 1;
          return true;
    }

    if (goTo(posRotate, posX))
    {
        moveNum++;
    }

    return false;
}
