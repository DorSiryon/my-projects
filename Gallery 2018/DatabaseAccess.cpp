#include <io.h>
#include "DatabaseAccess.h"

bool DatabaseAccess::execQuery(std::string sqlStatement)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(_database, sqlStatement.c_str(), nullptr, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
		return false;
	}

	return true;
}

bool DatabaseAccess::execQuery(std::string sqlStatement, int(*callback)(void*, int, char **, char **), void* container)
{
	char* errMessage = nullptr;
	int res = sqlite3_exec(_database, sqlStatement.c_str(), callback, container, &errMessage);

	if (res != SQLITE_OK)
	{
		std::cout << errMessage << std::endl;
		return false;
	}

	return true;
}


/*********************************** Album ***********************************/


const std::list<Album> DatabaseAccess::getAlbums()
{
	std::list<Album> albumsList;

	execQuery("SELECT * FROM ALBUMS;", albumsHandler, &albumsList);

	for (Album& album : albumsList)
	{
		album.syncPictures(getAlbumPictures(album.getId()));
	}

	return albumsList;
}

const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	std::list<Album> albumsList;

	execQuery("SELECT * FROM ALBUMS "
		"WHERE USER_ID = " + std::to_string(user.getId()) + ";", albumsHandler, &albumsList);

	for (Album& album : albumsList)
	{
		album.syncPictures(getAlbumPictures(album.getId()));
	}

	return albumsList;
}

void DatabaseAccess::createAlbum(const Album& album)
{
	execQuery("INSERT INTO ALBUMS(NAME, USER_ID, CREATION_DATE) " //ID field is AUTO INCREMENT
		"VALUES('" + album.getName() + "', " + std::to_string(album.getOwnerId()) + ", '" + album.getCreationDate() + "');");
}

void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	Album album = openAlbum(albumName, userId);

	//delete all the pictures in the album + each one's tags
	for (Picture& picture : getAlbumPictures(album.getId()))
	{
		execQuery("DELETE FROM TAGS "
			"WHERE PICTURE_ID = " + std::to_string(picture.getId()) + ";");

		execQuery("DELETE FROM PICTURES "
			"WHERE ID = " + std::to_string(picture.getId()) + ";");
	}

	//delete the album
	execQuery("DELETE FROM ALBUMS "
		"WHERE ID = " + std::to_string(album.getId()) + ";");
}

bool DatabaseAccess::doesAlbumExist(const std::string& albumName, int userId)
{
	bool doesExist = false;

	execQuery("SELECT * FROM ALBUMS "
		"WHERE NAME = '" + albumName + "' "
		"AND USER_ID = " + std::to_string(userId) + ";", doesExistHandler, &doesExist);

	return doesExist;
}

Album DatabaseAccess::openAlbum(const std::string& albumName, int userId)
{
	Album album;

	execQuery("SELECT * FROM ALBUMS "
		"WHERE NAME = '" + albumName + "' "
		"AND USER_ID = " + std::to_string(userId) + ";", albumHandler, &album);

	album.syncPictures(getAlbumPictures(album.getId()));

	return album;
}

void DatabaseAccess::closeAlbum(Album&)
{
	// basically here we would like to delete the allocated memory we got from openAlbum
}

void DatabaseAccess::printAlbums()
{
	const std::list<Album> albumsList = getAlbums();

	if (albumsList.empty()) {
		throw MyException("There are no existing albums.");
	}

	std::cout << "Album list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const Album& album : albumsList) {
		std::cout << std::setw(5) << "* " << album;
	}
}


/*********************************** Picture ***********************************/


bool DatabaseAccess::doesPictureExist(int pictureId)
{
	bool doesExist = false;

	execQuery("SELECT * FROM PICTURES "
		"WHERE ID = " + std::to_string(pictureId) + ";", doesExistHandler, &doesExist);

	return doesExist;
}

void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, int userId, const Picture& picture)
{
	Album album = openAlbum(albumName, userId);

	execQuery("INSERT INTO PICTURES "
		"VALUES(" + std::to_string(picture.getId()) + ", '" + picture.getName() + "', '" + picture.getPath() + "', '" + picture.getCreationDate() + "', " + std::to_string(album.getId()) + ");");
}

void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, int userId, const std::string& pictureName)
{
	Album album = openAlbum(albumName, userId);
	Picture picture = getPicture(album.getId(), pictureName);

	//delete the tags of the picture
	execQuery("DELETE FROM TAGS "
		"WHERE PICTURE_ID = " + std::to_string(picture.getId()) + ";");

	//delete the picture
	execQuery("DELETE FROM PICTURES "
		"WHERE ID = " + std::to_string(picture.getId()) + ";");
}

void DatabaseAccess::tagUserInPicture(const std::string& albumName, int albumUserId, const std::string& pictureName, int userId)
{
	Album album = openAlbum(albumName, albumUserId);

	Picture picture = getPicture(album.getId(), pictureName);

	execQuery("INSERT INTO TAGS "
		"VALUES(" + std::to_string(picture.getId()) + "," + std::to_string(userId) + ");");
}

void DatabaseAccess::untagUserInPicture(const std::string& albumName, int albumUserId, const std::string& pictureName, int userId)
{
	Album album = openAlbum(albumName, albumUserId);

	Picture picture = getPicture(album.getId(), pictureName);

	execQuery("DELETE FROM TAGS "
		"WHERE PICTURE_ID = " + std::to_string(picture.getId()) +
		" AND USER_ID = " + std::to_string(userId) + ";");
}

Picture DatabaseAccess::getPicture(int pictureId)
{
	Picture picture;

	execQuery("SELECT * FROM PICTURES "
		"WHERE ID = " + std::to_string(pictureId) + ";", pictureHandler, &picture);

	picture.syncTags(getPictureTags(pictureId));

	return picture;
}

Picture DatabaseAccess::getPicture(int albumId, const std::string& pictureName)
{
	Picture picture;

	execQuery("SELECT * FROM PICTURES "
		"WHERE NAME = '" + pictureName + "' "
		"AND ALBUM_ID = " + std::to_string(albumId) + ";", pictureHandler, &picture);

	picture.syncTags(getPictureTags(picture.getId()));

	return picture;
}

std::list<Picture> DatabaseAccess::getAlbumPictures(int albumId)
{
	std::list<Picture> picturesOfAlbum;

	execQuery("SELECT * FROM PICTURES "
		"WHERE ALBUM_ID = " + std::to_string(albumId) + ";", picturesHandler, &picturesOfAlbum);

	for (Picture& picture : picturesOfAlbum)
	{
		picture.syncTags(getPictureTags(picture.getId()));
	}

	return picturesOfAlbum;
}

std::set<int> DatabaseAccess::getPictureTags(int pictureId)
{
	std::set<int> tags;

	execQuery("SELECT TAGS.USER_ID FROM PICTURES "
		"INNER JOIN TAGS ON PICTURES.ID = TAGS.PICTURE_ID "
		"WHERE ID = " + std::to_string(pictureId) + ";", userIdHandler, &tags);

	return tags;
}


/*********************************** User ***********************************/


void DatabaseAccess::printUsers()
{
	std::list<User> users;

	execQuery("SELECT * FROM USERS;", usersHandler, &users);

	std::cout << "Users list:" << std::endl;
	std::cout << "-----------" << std::endl;
	for (const User& user : users) {
		std::cout << user << std::endl;
	}
}

void DatabaseAccess::createUser(User& user)
{
	execQuery("INSERT INTO USERS "
		"VALUES(" + std::to_string(user.getId()) + ", '" + user.getName() + "');");
}

void DatabaseAccess::deleteUser(const User& user)
{
	execQuery("DELETE FROM USERS "
		"WHERE id = " + std::to_string(user.getId()) + ";");
}

bool DatabaseAccess::doesUserExist(int userId)
{
	bool doesExist = false;

	execQuery("SELECT * FROM USERS "
		"WHERE ID = " + std::to_string(userId) + ";", doesExistHandler, &doesExist);

	return doesExist;
}

User DatabaseAccess::getUser(int userId)
{
	User user;

	execQuery("SELECT * FROM USERS "
		"WHERE ID = " + std::to_string(userId) + ";", userHandler, &user);

	return user;
}


/*********************************** User Statistics ***********************************/


int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	int numAlbumsOfUser = 0;

	execQuery("SELECT COUNT(*) FROM ALBUMS "
		"WHERE USER_ID = " + std::to_string(user.getId()) + ";", countHandler, &numAlbumsOfUser);

	return numAlbumsOfUser;
}

int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	std::vector<int> albumIds;

	//receive the album ids that the user is tagged in their pictures.
	execQuery("SELECT DISTINCT ALBUM_ID FROM TAGS "
		"INNER JOIN PICTURES ON PICTURES.ID = TAGS.PICTURE_ID "
		"WHERE TAGS.USER_ID = " + std::to_string(user.getId()) + ";", albumIdHandler, &albumIds);

	return albumIds.size();
}

int DatabaseAccess::countTagsOfUser(const User& user)
{
	int numTagsOfUser = 0;

	execQuery("SELECT COUNT(*) FROM TAGS "
		"WHERE USER_ID = " + std::to_string(user.getId()) + ";", countHandler, &numTagsOfUser);

	return numTagsOfUser;
}

float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	int albumsTaggedCount = countAlbumsTaggedOfUser(user);

	if (0 == albumsTaggedCount) {
		return 0;
	}

	return static_cast<float>(countTagsOfUser(user)) / albumsTaggedCount;
}


/*********************************** Queries ***********************************/


User DatabaseAccess::getTopTaggedUser()
{
	int userId = 0;

	execQuery("SELECT USER_ID, COUNT(*) FROM TAGS "
		"GROUP BY USER_ID "
		"ORDER BY COUNT(*) DESC "
		"LIMIT 1;", topCountHandler, &userId);

	return getUser(userId);
}

Picture DatabaseAccess::getTopTaggedPicture()
{
	int pictureId = 0;

	execQuery("SELECT PICTURE_ID, COUNT(*) FROM TAGS "
		"GROUP BY PICTURE_ID "
		"ORDER BY COUNT(*) DESC "
		"LIMIT 1;", topCountHandler, &pictureId);

	return getPicture(pictureId);
}

std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	std::list<Picture> picturesList;

	execQuery("SELECT * FROM TAGS "
		"INNER JOIN PICTURES ON TAGS.PICTURE_ID = PICTURES.ID "
		"WHERE TAGS.USER_ID = " + std::to_string(user.getId()) + ";", picturesHandler, &picturesList);

	for (Picture& picture : picturesList)
	{
		picture.syncTags(getPictureTags(picture.getId()));
	}

	return picturesList;
}


/*********************************** Database Functions ***********************************/


bool DatabaseAccess::open()
{
	int doesFileExist = _access(GALLERY_DB, 0);
	int res = sqlite3_open(GALLERY_DB, &_database);

	if (res != SQLITE_OK)
	{
		_database = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}

	if (doesFileExist == -1)
	{
		execQuery("BEGIN TRANSACTION;");

		try
		{
			//table USERS
			execQuery("CREATE TABLE `USERS` ( "
				"`ID`				INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
				"`NAME`				TEXT NOT NULL);");

			//table ALBUMS
			execQuery("CREATE TABLE `ALBUMS` ( "
				"`ID`				INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
				"`NAME`				TEXT NOT NULL, "
				"`USER_ID`			INTEGER NOT NULL, "
				"`CREATION_DATE`	TEXT NOT NULL, "

				"FOREIGN KEY(`USER_ID`) REFERENCES `USERS`(`ID`));");

			//table PICTURES
			execQuery("CREATE TABLE `PICTURES` ( "
				"`ID`				INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, "
				"`NAME`				TEXT NOT NULL, "
				"`LOCATION`			TEXT NOT NULL, "
				"`CREATION_DATE`	TEXT NOT NULL, "
				"`ALBUM_ID`			INTEGER NOT NULL, "

				"FOREIGN KEY(`ALBUM_ID`) REFERENCES `ALBUMS`(`ID`));");

			//table TAGS
			execQuery("CREATE TABLE `TAGS` ( "
				"`PICTURE_ID`		INTEGER NOT NULL, "
				"`USER_ID`			INTEGER NOT NULL, "

				"PRIMARY KEY(`PICTURE_ID`,`USER_ID`), "
				"FOREIGN KEY(`USER_ID`) REFERENCES `USERS`(`ID`), "
				"FOREIGN KEY(`PICTURE_ID`) REFERENCES `PICTURES`(`ID`));");

			execQuery("COMMIT;");
		}
		catch (std::exception& error)
		{
			std::cout << error.what() << std::endl;
			execQuery("ROLLBACK;");
		}
	}

	return true;
}

void DatabaseAccess::close()
{
	sqlite3_close(_database);
	_database = nullptr;
}

void DatabaseAccess::clear()
{

}


/*********************************** Callback Handlers ***********************************/

/**
*	CALLBACK FUNCTION:
*initiallizes an album object.
*
*1st arg input:
*	Album*
*/
int albumHandler(void* album, int argc, char** argv, char** azColName)
{
	int i = 0;
	Album* casted_album = (Album*)album;

	for (i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			casted_album->setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			casted_album->setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "USER_ID")
		{
			casted_album->setOwner(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			casted_album->setCreationDate(argv[i]);
		}
	}

	return 0;
}

/**
*	CALLBACK FUNCTION:
*initiallizes album objects and adds them to the albums list.
*
*1st arg input:
*	std::list<Album>*
*/
int albumsHandler(void* albumsList, int argc, char** argv, char** azColName)
{
	int i = 0;
	Album album;

	std::list<Album>* casted_albumList = (std::list<Album>*)albumsList;

	for (i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			album.setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			album.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "USER_ID")
		{
			album.setOwner(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			album.setCreationDate(argv[i]);
		}
	}
	casted_albumList->push_back(album);
	return 0;
}


/**
*	CALLBACK FUNCTION:
*initiallizes a picture object.
*
*1st arg input:
*	Picture*
*/
int pictureHandler(void* picture, int argc, char** argv, char** azColName)
{
	int i = 0;
	Picture* casted_picture = (Picture*)picture;

	for (i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			casted_picture->setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			casted_picture->setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION")
		{
			casted_picture->setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			casted_picture->setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "ALBUM_ID")
		{
			casted_picture->setAlbumId(atoi(argv[i]));
		}
	}
	return 0;
}

/**
*	CALLBACK FUNCTION:
*initiallizes picture objects and adds them to the pictures list.
*
*1st arg input:
*	std::list<Picture>*
*/
int picturesHandler(void* picturesList, int argc, char** argv, char** azColName)
{
	int i = 0;
	Picture picture;
	std::list<Picture>* casted_picturesList = (std::list<Picture>*)picturesList;

	for (i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			picture.setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			picture.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "LOCATION")
		{
			picture.setPath(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			picture.setCreationDate(argv[i]);
		}
		else if (std::string(azColName[i]) == "ALBUM_ID")
		{
			picture.setAlbumId(atoi(argv[i]));
		}
	}
	casted_picturesList->push_back(picture);
	return 0;
}

/**
*	CALLBACK FUNCTION:
*initiallizes an user object.
*
*1st arg input:
*	User*
*/
int userHandler(void* user, int argc, char** argv, char** azColName)
{
	int i = 0;
	User* casted_user = (User*)user;

	for (i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			casted_user->setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			casted_user->setName(argv[i]);
		}
	}
	return 0;
}

/**
*	CALLBACK FUNCTION:
*initiallizes user objects and adds them to the users list.
*
*1st arg input:
*	std::list<User>*
*/
int usersHandler(void* usersList, int argc, char** argv, char** azColName)
{
	int i = 0;
	User user;

	std::list<User>* casted_usersList = (std::list<User>*)usersList;

	for (i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "ID")
		{
			user.setId(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NAME")
		{
			user.setName(argv[i]);
		}
	}
	casted_usersList->push_back(user);
	return 0;
}

/**
*	CALLBACK FUNCTION:
*checks if the callback function is called.
*
*1st arg input:
*	bool*
*/
int doesExistHandler(void* doesExist, int argc, char** argv, char** azColName)
{
	bool* casted_doesExist = (bool*)doesExist;

	*casted_doesExist = true;

	return 0;
}

/**
*	CALLBACK FUNCTION:
*gets the SQL COUNT() function value.
**Must* use COUNT(*) and *not* COUNT(value)
*
*1st arg input:
*	int*
*/
int countHandler(void* countNum, int argc, char** argv, char** azColName)
{
	int* casted_countNum = (int*)countNum;

	if (argc == 1)
	{
		if (std::string(azColName[0]) == "COUNT(*)")
		{
			*casted_countNum = atoi(argv[0]);
		}
	}

	return 0;
}

/**
*	CALLBACK FUNCTION:
*adds the album IDs to a vector.
*
*1st arg input:
*	std::vector<int>*
*/
int albumIdHandler(void* albumIds, int argc, char** argv, char** azColName)
{
	std::vector<int>* casted_albumIds = (std::vector<int>*)albumIds;

	if (argc == 1)
	{
		if (std::string(azColName[0]) == "ALBUM_ID")
		{
			casted_albumIds->push_back(atoi(argv[0]));
		}
	}

	return 0;
}

/**
*	CALLBACK FUNCTION:
*gets the id field while reveiving the top count queries.
**Must* use COUNT(*) and *not* COUNT(value)
*
*1st arg input:
*	int*
*/
int topCountHandler(void* id, int argc, char** argv, char** azColName)
{
	int* casted_id = (int*)id;

	if (argc == 2) //should be EXACTLY 2 columns: ID, COUNT(*)
	{
		if (std::string(azColName[1]) == "COUNT(*)")
		{
			*casted_id = atoi(argv[0]);
		}
	}

	return 0;
}

/**
*	CALLBACK FUNCTION:
*adds the user IDs to a set.
*
*1st arg input:
*	std::set<int>*
*/
int userIdHandler(void* userIds, int argc, char** argv, char** azColName)
{
	std::set<int>* casted_userIds = (std::set<int>*)userIds;

	if (argc == 1)
	{
		if (std::string(azColName[0]) == "USER_ID")
		{
			casted_userIds->insert(atoi(argv[0]));
		}
	}

	return 0;
}