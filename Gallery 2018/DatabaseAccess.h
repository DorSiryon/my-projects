#pragma once
#include <list>
#include <vector>
#include <set>
#include "IDataAccess.h"
#include "sqlite3.h"
#include "Album.h"
#include "User.h"
#include "ItemNotFoundException.h"

#define GALLERY_DB "galleryDB.sqlite"

int albumsHandler(void* albumsList, int argc, char** argv, char** azColName);
int albumHandler(void* album, int argc, char** argv, char** azColName);
int pictureHandler(void* picture, int argc, char** argv, char** azColName);
int picturesHandler(void* picturesList, int argc, char** argv, char** azColName);
int userHandler(void* user, int argc, char** argv, char** azColName);
int usersHandler(void* usersList, int argc, char** argv, char** azColName);
int countHandler(void* countNum, int argc, char** argv, char** azColName);
int albumIdHandler(void* albumIds, int argc, char** argv, char** azColName);
int userIdHandler(void* userIds, int argc, char** argv, char** azColName);
int topCountHandler(void* id, int argc, char** argv, char** azColName);
int doesExistHandler(void* doesExist, int argc, char** argv, char** azColName);

class DatabaseAccess : public IDataAccess
{

public:
	DatabaseAccess() = default;
	virtual ~DatabaseAccess() = default;

	// album related
	const std::list<Album> getAlbums() override;
	const std::list<Album> getAlbumsOfUser(const User& user) override;
	void createAlbum(const Album& album) override;
	void deleteAlbum(const std::string& albumName, int userId) override;
	bool doesAlbumExist(const std::string& albumName, int userId) override;
	Album openAlbum(const std::string& albumName, int userId) override;
	void closeAlbum(Album &pAlbum) override;
	void printAlbums() override;

	// picture related
	bool doesPictureExist(int pictureId) override;
	void addPictureToAlbumByName(const std::string& albumName, int userId, const Picture& picture) override;
	void removePictureFromAlbumByName(const std::string& albumName, int userId, const std::string& pictureName) override;
	void tagUserInPicture(const std::string& albumName, int albumUserId, const std::string& pictureName, int userId) override;
	void untagUserInPicture(const std::string& albumName, int albumUserId, const std::string& pictureName, int userId) override;

	// user related
	void printUsers() override;
	void createUser(User& user) override;
	void deleteUser(const User& user) override;
	bool doesUserExist(int userId) override;
	User getUser(int userId) override;

	// user statistics
	int countAlbumsOwnedOfUser(const User& user) override;
	int countAlbumsTaggedOfUser(const User& user) override;
	int countTagsOfUser(const User& user) override;
	float averageTagsPerAlbumOfUser(const User& user) override;

	// queries
	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;

	// Database Functions
	bool open() override;
	void close() override;
	void clear() override;

private:
	sqlite3* _database;

	Picture getPicture(int pictureId);
	Picture getPicture(int albumId, const std::string& pictureName);
	std::set<int> getPictureTags(int pictureId);
	std::list<Picture> getAlbumPictures(int albumId);

	bool execQuery(std::string sqlStatement);
	bool execQuery(std::string sqlStatement, int(*callback)(void*, int, char **, char **), void* container);
};
