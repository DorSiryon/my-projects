#pragma once
#include <vector>
#include <Windows.h>
#include <atlstr.h>
#include "Constants.h"
#include "MemoryAccess.h"
#include "Album.h"

enum programOptions
{
	opt_paint = 1,
	opt_photoshop,
	opt_irfanView
};

#define PAINT "mspaint.exe"
#define PHOTOSHOP "C:\\Program Files\\Adobe\\Adobe Photoshop CC 2015\\Photoshop.exe"
#define IRFAN_VIEW "C:\\Program Files\\IrfanView\\i_view64.exe"

#define PATHS_TCHAR_SIZE 1024

class MemoryAccess;
class DatabaseAccess;
class AlbumManager
{
public:
	AlbumManager(IDataAccess& dataAccess);

	void executeCommand(CommandType command);
	void printHelp() const;

	using handler_func_t = void (AlbumManager::*)(void);    

private:
    int m_nextPictureId{};
    int m_nextUserId{};
    std::string m_currentAlbumName{};
	IDataAccess& m_dataAccess;
	Album m_openAlbum;

	void help();
	// albums management
	void createAlbum();
	void openAlbum();
	void closeAlbum();
	void deleteAlbum();
	void listAlbums();
	void listAlbumsOfUser();

	// Picture management
	void addPictureToAlbum();
	void removePictureFromAlbum();
	void listPicturesInAlbum();
	void showPicture();
	int openPictureInProgram(const std::string& program, const std::string& picPath);

	int getFileLastWrite(const std::string& picPath, FILETIME* lastChanged);
	void printProgramsList();

	// tags related
	void tagUserInPicture();
	void untagUserInPicture();
	void listUserTags();

	// users management
	void addUser();
	void removeUser();
	void listUsers();
	void userStatistics();

	void topTaggedUser();
	void topTaggedPicture();
	void picturesTaggedUser();
	void exit();

	std::string getInputFromConsole(const std::string& message);
	bool fileExistsOnDisk(const std::string& filename);
	void refreshOpenAlbum();
    bool isCurrentAlbumSet() const;

	static const std::vector<struct CommandGroup> m_prompts;
	static const std::map<CommandType, handler_func_t> m_commands;

};

